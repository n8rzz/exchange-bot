import { InjectQueue } from '@nestjs/bull';
import { Injectable, Logger } from '@nestjs/common';
import { JobOptions, Queue } from 'bull';
import { QueueName } from './queue.constants';
import {
  IndicatorName,
  StrategySegmentType,
} from '../domain/strategy-segments/strategy-segments.constants';
import { translatePeriodToDelayInterval } from '../utils/time.utils';
import { Strategy } from '../domain/strategies/models/strategy.entity';
import {
  ICreateScreategySchedulesData,
  IGetCandlesForTickerJobData,
} from './queue.types';

@Injectable()
export class QueueService {
  private readonly _logger = new Logger(QueueService.name);

  constructor(
    @InjectQueue(QueueName.Bot) private _botQueue: Queue,
    @InjectQueue(QueueName.Exchange) private _exchangeQueue: Queue,
    @InjectQueue(QueueName.Strategy) private _strategyQueue: Queue,
  ) {}

  /**
   * Starts job queue for a bot
   *
   * - collect unique periods and each ticker
   * - schedule ohlcv refresh for unique periods and tickers
   */
  async createStrategySchedules(
    botId: string,
    exchangeId: string,
    strategyId: string,
    tickers: string[],
  ): Promise<void> {
    const jobData: ICreateScreategySchedulesData = {
      botId,
      exchangeId,
      strategyId,
      tickers,
    };

    await this._strategyQueue.add(jobData);
  }

  /**
   *
   */
  async scheduleCandlestickRefresh(
    botId: string,
    exchangeId: string,
    strategy: Strategy,
    ticker: string,
    period: number,
    indicators: IndicatorName[],
  ): Promise<void> {
    this._logger.log(
      `botId: ${botId}  ticker: ${ticker}  period: ${period}`,
      'QueueService.scheduleCandlestickRefresh',
    );

    const jobData: IGetCandlesForTickerJobData = {
      botId,
      exchangeId,
      strategy,
      ticker,
      period,
      indicators,
    };
    const options: JobOptions = {
      jobId: `candle-refresh-${botId}-${ticker}`,
      repeat: {
        every: translatePeriodToDelayInterval(period),
      },
    };

    await this._exchangeQueue.add('candle-refresh', jobData, options);
  }

  async refreshSignalsForBot(botId: string): Promise<void> {
    await this._botQueue.add({
      botId,
    });
  }

  async generateOrderForBotSignal(
    botId: string,
    ticker: string,
    type: StrategySegmentType,
  ): Promise<void> {
    this._logger.log(
      `botId: ${botId}  ticker: ${ticker}  type: ${type}`,
      'QueueService.generateOrderForBotSignal',
    );
    // TODO: https://gitlab.com/n8rzz/exchange-bot/-/issues/13
  }

  /**
   *
   */
  async destroyStrategySchedules(botId: string): Promise<void> {
    this._logger.log(botId, 'QueueService.destroyCandleRefreshJobsForBotId');

    try {
      const candleRefreshJobs = await this._exchangeQueue.getRepeatableJobs();

      return candleRefreshJobs
        .filter((job) => job.id?.includes(`candle-refresh-${botId}`))
        .forEach(async (job) => {
          this._logger.log(
            `jobId: candle-refresh-${botId}  key: ${job.key}`,
            `QueueService.removeRepeatableByKey`,
          );
          this._exchangeQueue.removeRepeatableByKey(job.key);
        });
    } catch (error) {
      this._logger.error(error);

      throw error;
    }
  }
}

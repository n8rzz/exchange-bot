export enum QueueName {
  Bot = 'Bot',
  Exchange = 'Exchange',
  Strategy = 'Strategy',
}

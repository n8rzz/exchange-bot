import { Router } from 'express';
import * as dotenv from 'dotenv';
import { BullModule, InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';
import { createBullBoard } from 'bull-board';
import { BullAdapter } from 'bull-board/bullAdapter';
import { MiddlewareConsumer, Module } from '@nestjs/common';
import { QueueName } from './queue.constants';
import { QueueService } from './queue.service';
import { StrategiesModule } from '../domain/strategies/strategies.module';
import { ExchangesModule } from '../domain/exchanges/exchanges.module';
import { TechnicalAnalysisModule } from '../domain/technical-analysis/technical-analysis.module';

dotenv.config();

@Module({
  imports: [
    ExchangesModule,
    StrategiesModule,
    TechnicalAnalysisModule,
    BullModule.registerQueueAsync({
      name: QueueName.Bot,
      useFactory: async () => ({
        name: QueueName.Bot,
        settings: {
          lockDuration: 300000,
        },
      }),
    }),
    BullModule.registerQueueAsync({
      name: QueueName.Exchange,
      useFactory: async () => ({
        name: QueueName.Exchange,
        settings: {
          lockDuration: 300000,
        },
      }),
    }),
    BullModule.registerQueueAsync({
      name: QueueName.Strategy,
      useFactory: async () => ({
        name: QueueName.Strategy,
        settings: {
          lockDuration: 300000,
        },
      }),
    }),
  ],
  providers: [QueueService],
  exports: [QueueService, BullModule],
})
export class QueueModule {
  private _router: Router;

  constructor(
    @InjectQueue(QueueName.Bot) private readonly _botQueue: Queue,
    @InjectQueue(QueueName.Exchange) private readonly _marketInfoQueue: Queue,
    @InjectQueue(QueueName.Strategy) private readonly _strategyQueue: Queue,
  ) {
    const { router } = createBullBoard([
      new BullAdapter(this._botQueue, { readOnlyMode: false }),
      new BullAdapter(this._marketInfoQueue, { readOnlyMode: false }),
      new BullAdapter(this._strategyQueue, { readOnlyMode: false }),
    ]);

    this._router = router;
  }

  configure(consumer: MiddlewareConsumer): void {
    consumer.apply(this._router).forRoutes('/admin/queues');
  }
}

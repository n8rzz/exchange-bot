import * as ccxt from 'ccxt';
import { Strategy } from '../domain/strategies/models/strategy.entity';
import { IndicatorName } from '../domain/strategy-segments/strategy-segments.constants';

export interface IRefreshSignalsForBotJobData {
  botId: string;
}

export interface ICreateScreategySchedulesData {
  botId: string;
  exchangeId: string;
  strategyId: string;
  tickers: string[];
}

export interface IGetCandlesForTickerJobData {
  botId: string;
  exchangeId: string;
  indicators: IndicatorName[];
  period: number;
  strategy: Strategy;
  ticker: string;
}

export interface ITechnicalAnalysisJobData {
  botId: string;
  candles: ccxt.OHLCV[];
  indicators: IndicatorName;
  period: number;
  strategyId: string;
  ticker: string;
}

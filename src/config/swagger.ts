import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ApiTag } from './swagger.constants';

export function buildSwaggerConfig(app: INestApplication) {
  const swaggerConfig = new DocumentBuilder()
    .setTitle('exchange-bot Api')
    // .setDescription('Habitude api documentation')
    .setVersion('1.0')
    .addTag(ApiTag.Accounts)
    .addTag(ApiTag.Authentication)
    .addTag(ApiTag.Bots)
    .addTag(ApiTag.Exchanges)
    .addTag(ApiTag.Orders)
    .addTag(ApiTag.Signals)
    .addTag(ApiTag.Strategies)
    .addTag(ApiTag.StrategySegments)
    .addTag(ApiTag.Users)
    .addServer('http://localhost:3000')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, swaggerConfig, {
    ignoreGlobalPrefix: true,
  });

  SwaggerModule.setup('swagger', app, document);
}

export enum ApiTag {
  Accounts = 'accounts',
  Authentication = 'authentication',
  Bots = 'bots',
  Exchanges = 'exchanges',
  Orders = 'orders',
  Signals = 'signals',
  Strategies = 'strategies',
  StrategySegments = 'strategy-segments',
  Users = 'users',
}

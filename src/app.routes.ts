import { Routes } from 'nest-router';
import { BotsModule } from './domain/bots/bots.module';
import { OrdersModule } from './domain/orders/orders.module';
import { SignalsModule } from './domain/signals/signals.module';
import { StrategiesModule } from './domain/strategies/strategies.module';
import { StrategySegmentsModule } from './domain/strategy-segments/strategy-segments.module';
import { UserExchangeAccountsModule } from './domain/user-exchange-accounts/user-exchange-accounts.module';
import { UsersModule } from './domain/users/users.module';

const routeBase = 'api';

export const routes: Routes = [
  {
    path: `${routeBase}/users`,
    module: UsersModule,
    children: [
      {
        path: '/:userId/accounts',
        module: UserExchangeAccountsModule,
      },
      {
        path: '/:userId/orders',
        module: OrdersModule,
      },
    ],
  },
  {
    path: `${routeBase}/strategies`,
    module: StrategiesModule,
    children: [
      {
        path: '/:strategyId/segments',
        module: StrategySegmentsModule,
      },
    ],
  },
  {
    path: `${routeBase}/bots`,
    module: BotsModule,
    children: [
      {
        path: '/:botId/signals',
        module: SignalsModule,
      },
    ],
  },
];

import { RouterModule } from 'nest-router';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './database/database.module';
import { UsersModule } from './domain/users/users.module';
import { AuthModule } from './domain/auth/auth.module';
import { StrategiesModule } from './domain/strategies/strategies.module';
import { StrategySegmentsModule } from './domain/strategy-segments/strategy-segments.module';
import { routes } from './app.routes';
import { BotsModule } from './domain/bots/bots.module';
import { QueueModule } from './queue/queue.module';
import { BullModule } from '@nestjs/bull';
import { ExchangesModule } from './domain/exchanges/exchanges.module';
import { TechnicalAnalysisModule } from './domain/technical-analysis/technical-analysis.module';
import { SignalsModule } from './domain/signals/signals.module';
import { UserExchangeAccountsModule } from './domain/user-exchange-accounts/user-exchange-accounts.module';
import { OrdersModule } from './domain/orders/orders.module';

@Module({
  imports: [
    RouterModule.forRoutes(routes),
    ConfigModule.forRoot({ isGlobal: true }),
    BullModule.forRoot({
      redis: {
        host: process.env.REDIS_HOST,
        port: process.env.REDIS_PORT as any,
      },
    }),
    QueueModule,
    DatabaseModule,
    UsersModule,
    AuthModule,
    StrategiesModule,
    StrategySegmentsModule,
    BotsModule,
    ExchangesModule,
    TechnicalAnalysisModule,
    SignalsModule,
    UserExchangeAccountsModule,
    OrdersModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

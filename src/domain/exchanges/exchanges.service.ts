import { Inject, Injectable } from '@nestjs/common';
import * as cctx from 'ccxt';
import { Repository } from '../../constants/repository.constants';
import { periodToCandleTime } from '../../constants/time.constants';
import { ExchangeName } from './exchanges.constants';
import { CreateExchangeDto, ExchangeDto } from './models/exchange.dto';
import { Exchange } from './models/exchange.entity';

@Injectable()
export class ExchangesService {
  private _binanceUs: cctx.Exchange = null;

  get exchange(): { [key: string]: cctx.Exchange } {
    return {
      [ExchangeName.Binanceus]: this._binanceUs,
    };
  }

  constructor(
    @Inject(Repository.Exchange)
    private readonly _exchangeRepository: typeof Exchange,
  ) {
    this._binanceUs = new cctx[ExchangeName.Binanceus]({
      enableRateLimit: true,
    });

    this._binanceUs.setSandboxMode(true);
  }

  async getExchanges(): Promise<unknown> {
    return cctx.exchanges;
  }

  async getTickers(id: string): Promise<cctx.Dictionary<cctx.Ticker>> {
    const exchange = await this.findOne(id);

    return this.exchange[exchange.name].fetchTickers();
  }

  async getCapabilities(id: string): Promise<unknown> {
    const exchange = await this.findOne(id);

    return this.exchange[exchange.name].describe();
  }

  async getCandleData(
    ticker: string,
    period: number,
    exchange: ExchangeName,
  ): Promise<cctx.OHLCV[]> {
    const candleTime = periodToCandleTime[period];

    return this.exchange[exchange].fetchOHLCV(ticker, candleTime);
  }

  async getMarkets(id: string): Promise<unknown> {
    const exchange = await this.findOne(id);

    return this.exchange[exchange.name].fetchMarkets();
  }

  // ---

  async create(exchange: CreateExchangeDto): Promise<Exchange> {
    return this._exchangeRepository.create<Exchange>(exchange);
  }

  async findAll(): Promise<Exchange[]> {
    return this._exchangeRepository.findAll<Exchange>();
  }

  async findOne(id: string): Promise<Exchange> {
    return this._exchangeRepository.findOne({
      where: { id },
    });
  }

  async delete(id: string): Promise<number> {
    return this._exchangeRepository.destroy({ where: { id } });
  }

  async update(id: string, data: Partial<ExchangeDto>) {
    const [numberOfAffectedRows, [updatedBot]] =
      await this._exchangeRepository.update(
        { ...data },
        { where: { id }, returning: true },
      );

    return { numberOfAffectedRows, updatedBot };
  }
}

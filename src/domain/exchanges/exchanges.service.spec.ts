import { Test, TestingModule } from '@nestjs/testing';
import { Repository } from '../../constants/repository.constants';
import { ExchangesService } from './exchanges.service';

describe('ExchangesService', () => {
  let service: ExchangesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ExchangesService,
        {
          provide: Repository.Exchange,
          useValue: jest.fn(),
        },
      ],
    }).compile();

    service = module.get<ExchangesService>(ExchangesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  NotFoundException,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBearerAuth,
  ApiTags,
  ApiOperation,
  ApiOkResponse,
  ApiBadRequestResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiCreatedResponse,
  ApiConflictResponse,
  ApiForbiddenResponse,
} from '@nestjs/swagger';
import { ApiTag } from '../../config/swagger.constants';
import { ExchangesService } from './exchanges.service';
import { CreateExchangeDto, ExchangeDto } from './models/exchange.dto';
import { Exchange } from './models/exchange.entity';

@ApiBearerAuth()
@ApiTags(ApiTag.Exchanges)
@Controller('api/exchanges')
export class ExchangesController {
  constructor(private readonly _exchangesService: ExchangesService) {}

  @UseGuards(AuthGuard('jwt'))
  @Get('/all')
  @ApiOperation({
    summary: 'list exchange capabilities (from ccxt library)',
    externalDocs: {
      description: 'ccxt unified api documentation',
      url: 'https://github.com/ccxt/ccxt/wiki/Manual#unified-api',
    },
  })
  @ApiOkResponse()
  @ApiBadRequestResponse({ description: 'Bad request' })
  @ApiNotFoundResponse({ description: 'Not found' })
  async getExchanges() {
    return this._exchangesService.getExchanges();
  }

  @UseGuards(AuthGuard('jwt'))
  @Get(':id/capabilities')
  @ApiOperation({
    summary: 'list exchange capabilities (from ccxt library)',
    externalDocs: {
      description: 'ccxt unified api documentation',
      url: 'https://github.com/ccxt/ccxt/wiki/Manual#unified-api',
    },
  })
  @ApiOkResponse()
  @ApiBadRequestResponse({ description: 'Bad request' })
  @ApiNotFoundResponse({ description: 'Not found' })
  async getCapabilities(@Param('id') id: string) {
    return this._exchangesService.getCapabilities(id);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get(':id/tickers')
  @ApiOperation({
    summary: 'list available tickers',
    externalDocs: {
      description: 'ccxt unified api documentation',
      url: 'https://github.com/ccxt/ccxt/wiki/Manual#unified-api',
    },
  })
  @ApiOkResponse()
  @ApiBadRequestResponse({ description: 'Bad request' })
  @ApiNotFoundResponse({ description: 'Not found' })
  async getTickers(@Param('id') id: string) {
    return this._exchangesService.getTickers(id);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get(':id/markets')
  @ApiOperation({
    summary: 'list markets',
    externalDocs: {
      description: 'ccxt unified api documentation',
      url: 'https://github.com/ccxt/ccxt/wiki/Manual#unified-api',
    },
  })
  @ApiOkResponse()
  @ApiBadRequestResponse({ description: 'Bad request' })
  @ApiNotFoundResponse({ description: 'Not found' })
  async getMarkets(@Param('id') id: string) {
    return this._exchangesService.getMarkets(id);
  }

  // ---

  @UseGuards(AuthGuard('jwt'))
  @Get()
  @ApiOperation({ summary: 'list exchanges' })
  @ApiOkResponse({
    type: ExchangeDto,
    isArray: true,
  })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  async findAll() {
    return this._exchangesService.findAll();
  }

  @UseGuards(AuthGuard('jwt'))
  @Get(':id')
  @ApiOperation({ summary: 'find exchange' })
  @ApiOkResponse({
    type: ExchangeDto,
  })
  async findOne(@Param('id') id: string): Promise<Exchange> {
    const post = await this._exchangesService.findOne(id);

    if (!post) {
      throw new NotFoundException("This Post doesn't exist");
    }

    return post;
  }

  @UseGuards(AuthGuard('jwt'))
  @Post()
  @ApiOperation({ summary: 'create exchange' })
  @ApiCreatedResponse({
    type: ExchangeDto,
  })
  @ApiBadRequestResponse({ description: 'Bad request' })
  @ApiNotFoundResponse({ description: 'Not found' })
  @ApiConflictResponse({ description: 'Conflict' })
  async create(@Body() exchange: CreateExchangeDto): Promise<Exchange> {
    return this._exchangesService.create(exchange);
  }

  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'update exchange' })
  @Put(':id')
  @ApiOkResponse({
    type: ExchangeDto,
  })
  @ApiBadRequestResponse({ description: 'Bad request' })
  @ApiBadRequestResponse({ description: 'Not found' })
  @ApiConflictResponse({ description: 'Conflict' })
  async update(
    @Param('id') id: string,
    @Body() exchange: ExchangeDto,
  ): Promise<Exchange> {
    const { numberOfAffectedRows, updatedBot } =
      await this._exchangesService.update(id, exchange);

    if (numberOfAffectedRows === 0) {
      throw new NotFoundException("Bot doesn't exist");
    }

    return updatedBot;
  }

  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'delete exchange' })
  @Delete(':id')
  @ApiNoContentResponse({
    description: 'Empty response',
  })
  @ApiBadRequestResponse({ description: 'Bad request' })
  @ApiNotFoundResponse({ description: 'Not found' })
  @HttpCode(204)
  async remove(@Param('id') id: string): Promise<string> {
    const deleted = await this._exchangesService.delete(id);

    if (deleted === 0) {
      throw new NotFoundException("Exchange doesn't exist");
    }

    return 'Successfully deleted';
  }
}

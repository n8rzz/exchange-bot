import { forwardRef, Module } from '@nestjs/common';
import { ExchangesService } from './exchanges.service';
import { ExchangesController } from './exchanges.controller';
import { ExchangeConsumer } from './exchange.consumer';
import { QueueModule } from '../../queue/queue.module';
import { TechnicalAnalysisModule } from '../technical-analysis/technical-analysis.module';
import { SignalsModule } from '../signals/signals.module';
import { exchangesProviders } from './exchanges.providers';

@Module({
  imports: [
    forwardRef(() => QueueModule),
    SignalsModule,
    TechnicalAnalysisModule,
  ],
  providers: [ExchangeConsumer, ExchangesService, ...exchangesProviders],
  controllers: [ExchangesController],
  exports: [ExchangesService],
})
export class ExchangesModule {}

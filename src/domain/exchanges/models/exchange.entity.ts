import { Column, DataType, HasMany, Model, Table } from 'sequelize-typescript';
import { Bot } from '../../bots/models/bot.entity';
import { ExchangeName } from '../exchanges.constants';
import { ExchangeDto } from './exchange.dto';

@Table
export class Exchange extends Model<ExchangeDto> {
  @Column({
    type: DataType.UUID,
    allowNull: false,
    autoIncrement: false,
    defaultValue: DataType.UUIDV4,
    primaryKey: true,
  })
  id: string;

  @Column({
    type: DataType.ENUM({
      values: [...Object.values(ExchangeName)],
    }),
    allowNull: false,
    unique: true,
  })
  name: ExchangeName;

  @HasMany(() => Bot)
  bots: Bot[];
}

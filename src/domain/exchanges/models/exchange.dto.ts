import { IsNotEmpty, IsUUID, IsEnum } from 'class-validator';
import { ExchangeName } from '../exchanges.constants';

export class CreateExchangeDto {
  @IsEnum(ExchangeName)
  @IsNotEmpty()
  readonly name: ExchangeName;
}

export class ExchangeDto extends CreateExchangeDto {
  @IsUUID()
  id?: string;
}

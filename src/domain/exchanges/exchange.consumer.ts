import { Processor, Process } from '@nestjs/bull';
import { Job } from 'bull';
import * as ccxt from 'ccxt';
import { QueueName } from '../../queue/queue.constants';
import { IndicatorName } from '../strategy-segments/strategy-segments.constants';
import { QueueService } from '../../queue/queue.service';
import { IGetCandlesForTickerJobData } from '../../queue/queue.types';
import { ExchangesService } from './exchanges.service';
import { ExchangeName } from './exchanges.constants';
import { TechnicalAnalysisService } from '../technical-analysis/technical-analysis.service';
import { CreateSignalDto } from '../signals/models/signal.dto';
import { SignalsService } from '../signals/signals.service';
import { Logger } from '@nestjs/common';

@Processor(QueueName.Exchange)
export class ExchangeConsumer {
  private readonly _logger = new Logger(ExchangeConsumer.name);

  constructor(
    private readonly _exchangesService: ExchangesService,
    private readonly _queueService: QueueService,
    private readonly _signalService: SignalsService,
    private readonly _technicalAnalysisService: TechnicalAnalysisService,
  ) {}

  @Process('candle-refresh')
  async getCandlesForTicker(job: Job<IGetCandlesForTickerJobData>) {
    this._logger.log(
      `botId: ${job.data.botId}  ticker: ${job.data.ticker}`,
      'ExchangeConsumer.getCandlesForTicker',
    );

    // TODO: pull from [future] bot.exchange
    const exchange = ExchangeName.Binanceus;
    const candles = await this._exchangesService.getCandleData(
      job.data.ticker,
      job.data.period,
      exchange,
    );
    const CLOSE_VALUE_INDEX = 4;
    const closePriceList = candles.map(
      (entry: ccxt.OHLCV) => entry[CLOSE_VALUE_INDEX],
    );
    const sharedSignalProps = {
      ticker: job.data.ticker,
      exchangeId: job.data.exchangeId,
      userId: job.data.strategy.userId,
    };

    // TODO: store candleData in redis with #period as expiration (${TICKER}-${PERIOD} as key?)

    job.data.indicators.forEach(async (indicator: IndicatorName) => {
      const taResult =
        this._technicalAnalysisService.generateSignalForIndicator(
          closePriceList,
          indicator,
          job.data.period,
          job.data.strategy.strategySegments,
        );
      const signal: CreateSignalDto = {
        ...sharedSignalProps,
        analysisTime: new Date(),
        indicatorName: indicator,
        isRequired: taResult.isRequired,
        params: taResult,
        signalResult: taResult.signal,
        strategySegmentId: taResult.strategySegmentId,
      };

      this._logger.log(
        `botId: ${job.data.botId}  strategySegmentId: ${taResult.strategySegmentId}  signal: ${signal.signalResult}`,
        'ExchangeConsumer.signal',
      );

      try {
        await this._signalService.create(job.data.botId, signal);
      } catch (error) {
        this._logger.error(error, 'ExchangeConsumer.signal');
      }
    });

    this._queueService.refreshSignalsForBot(job.data.botId);
  }
}

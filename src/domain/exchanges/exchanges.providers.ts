import { Repository } from '../../constants/repository.constants';
import { Exchange } from './models/exchange.entity';

export const exchangesProviders = [
  {
    provide: Repository.Exchange,
    useValue: Exchange,
  },
];

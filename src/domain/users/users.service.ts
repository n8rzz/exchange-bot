import { Injectable, Inject } from '@nestjs/common';
import { Repository } from '../../constants/repository.constants';
import { UserDto } from './models/user.dto';
import { User } from './models/user.entity';

export interface IUsersService {
  create: (user: UserDto) => Promise<User>;
  findOneByEmail: (email: string) => Promise<User>;
  findOneById: (id: string) => Promise<User>;
}

@Injectable()
export class UsersService implements IUsersService {
  constructor(
    @Inject(Repository.User) private readonly userRepository: typeof User,
  ) {}

  async create(user: UserDto): Promise<User> {
    return this.userRepository.create<User>(user);
  }

  async findOneByEmail(email: string): Promise<User> {
    return this.userRepository.findOne<User>({ where: { email } });
  }

  async findOneById(id: string): Promise<User> {
    return this.userRepository.findOne<User>({ where: { id } });
  }

  /**
   * @param id string
   * @returns User  without `password` field
   */
  async findOne(id: string): Promise<User> {
    return this.userRepository.findOne<User>({
      where: { id },
      attributes: { exclude: ['password'] },
    });
  }
}

import { Table, Column, Model, DataType } from 'sequelize-typescript';
import { UserDto } from './user.dto';

@Table
export class User extends Model<UserDto> {
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  name: string;

  @Column({
    type: DataType.STRING,
    unique: true,
    allowNull: false,
  })
  email: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  password: string;
}

import {
  IsNotEmpty,
  MinLength,
  IsEmail,
  IsString,
  IsUUID,
} from 'class-validator';

export class CreateUserDto {
  @IsString()
  @IsNotEmpty()
  readonly name: string;

  @IsNotEmpty()
  @IsEmail()
  readonly email: string;

  @IsNotEmpty()
  @MinLength(6)
  readonly password: string;
}

export class UserDto extends CreateUserDto {
  @IsUUID()
  readonly id?: string;
}

import {
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Request,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBearerAuth,
  ApiForbiddenResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { ApiTag } from '../../config/swagger.constants';
import { UserDto } from './models/user.dto';
import { UsersService } from './users.service';

@ApiBearerAuth()
@ApiTags(ApiTag.Users)
/**
 * path set via `app.routes`:
 *
 * `api/users`
 */
@Controller()
export class UsersController {
  constructor(private readonly _usersService: UsersService) {}

  @UseGuards(AuthGuard('jwt'))
  @Get(':id')
  @ApiOperation({ summary: 'list single users' })
  @ApiOkResponse({
    type: UserDto,
  })
  @ApiForbiddenResponse()
  async findOneById(@Param('id') id: string, @Request() req) {
    if (id !== req.user.id) {
      throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
    }

    return this._usersService.findOne(id);
  }
}

import { Repository } from '../../constants/repository.constants';
import { User } from './models/user.entity';

export const usersProviders = [
  {
    provide: Repository.User,
    useValue: User,
  },
];

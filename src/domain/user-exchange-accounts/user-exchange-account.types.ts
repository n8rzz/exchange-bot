export interface IUserExchangeAccount {
  apiKey: string;
  createdAt?: string;
  exchangeId: string;
  id?: string;
  password?: string;
  secret: string;
  uid?: string;
  updatedAt?: string;
  userId: string;
}

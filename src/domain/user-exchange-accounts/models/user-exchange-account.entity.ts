import {
  BelongsTo,
  Column,
  DataType,
  ForeignKey,
  Table,
  Model,
} from 'sequelize-typescript';
import { Exchange } from '../../exchanges/models/exchange.entity';
import { User } from '../../users/models/user.entity';
import { UserExchangeAccountDto } from './user-exchange-account.dto';

@Table
export class UserExchangeAccount extends Model<UserExchangeAccountDto> {
  @Column({
    type: DataType.UUID,
    allowNull: false,
    autoIncrement: false,
    defaultValue: DataType.UUIDV4,
    primaryKey: true,
  })
  id: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  apiKey: string;

  @Column({
    type: DataType.STRING,
    allowNull: true,
  })
  password?: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  secret: string;

  @Column({
    type: DataType.STRING,
    allowNull: true,
  })
  uid?: string;

  @ForeignKey(() => Exchange)
  @Column({
    type: DataType.UUID,
    allowNull: false,
  })
  exchangeId: string;

  @BelongsTo(() => Exchange)
  exchange: Exchange;

  @ForeignKey(() => User)
  @Column({
    type: DataType.UUID,
    allowNull: false,
  })
  userId: string;

  @BelongsTo(() => User)
  user: User;
}

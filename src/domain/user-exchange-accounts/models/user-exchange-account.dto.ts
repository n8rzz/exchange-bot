import { IsNotEmpty, IsString, IsUUID } from 'class-validator';
import { IUserExchangeAccount } from '../user-exchange-account.types';

export class CreateUserExchangeAccountDto implements IUserExchangeAccount {
  @IsString()
  @IsNotEmpty()
  public readonly apiKey: string;

  @IsUUID()
  @IsNotEmpty()
  public readonly exchangeId: string;

  @IsString()
  @IsNotEmpty()
  public readonly password: string;

  @IsString()
  @IsNotEmpty()
  public readonly secret: string;

  @IsString()
  @IsNotEmpty()
  public readonly uid: string;

  public readonly userId: string;
}

export class UserExchangeAccountDto extends CreateUserExchangeAccountDto {
  @IsUUID()
  id?: string;
}

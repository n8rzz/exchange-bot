import { Repository } from '../../constants/repository.constants';
import { UserExchangeAccount } from './models/user-exchange-account.entity';

export const userExchangeAccountsProviders = [
  {
    provide: Repository.UserExchangeAccount,
    useValue: UserExchangeAccount,
  },
];

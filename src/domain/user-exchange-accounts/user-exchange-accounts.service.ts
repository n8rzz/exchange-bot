import { Injectable, Inject } from '@nestjs/common';
import { Repository } from '../../constants/repository.constants';
import {
  CreateUserExchangeAccountDto,
  UserExchangeAccountDto,
} from './models/user-exchange-account.dto';
import { UserExchangeAccount } from './models/user-exchange-account.entity';

export interface IUserExchangeAccountsService {
  create: (
    account: CreateUserExchangeAccountDto,
    userId: string,
  ) => Promise<UserExchangeAccount>;
  findAll: (userId: string) => Promise<UserExchangeAccount[]>;
  findOne: (id: string, userId: string) => Promise<UserExchangeAccount>;
  delete: (id: string, userId: string) => Promise<number>;
  update: (
    id: string,
    userId: string,
    data: Partial<UserExchangeAccount>,
  ) => Promise<{
    numberOfAffectedRows: number;
    updatedAccount: UserExchangeAccount;
  }>;
}

@Injectable()
export class UserExchangeAccountsService
  implements IUserExchangeAccountsService
{
  constructor(
    @Inject(Repository.UserExchangeAccount)
    private readonly _accountRepository: typeof UserExchangeAccount,
  ) {}

  async create(
    account: CreateUserExchangeAccountDto,
    userId: string,
  ): Promise<UserExchangeAccount> {
    return this._accountRepository.create<UserExchangeAccount>({
      ...account,
      userId: userId,
    });
  }

  async findAll(userId: string): Promise<UserExchangeAccount[]> {
    return this._accountRepository.findAll<UserExchangeAccount>({
      where: { userId },
    });
  }

  async findOne(id: string, userId: string): Promise<UserExchangeAccount> {
    return this._accountRepository.findOne({
      where: { id, userId },
    });
  }

  async delete(id: string, userId: string): Promise<number> {
    return this._accountRepository.destroy({ where: { id, userId } });
  }

  async update(
    id: string,
    userId: string,
    data: Partial<UserExchangeAccountDto>,
  ) {
    const [numberOfAffectedRows, [updatedAccount]] =
      await this._accountRepository.update(
        { ...data },
        { where: { id, userId }, returning: true },
      );

    return { numberOfAffectedRows, updatedAccount };
  }
}

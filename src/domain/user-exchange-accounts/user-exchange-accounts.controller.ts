import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  NotFoundException,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { ApiTag } from '../../config/swagger.constants';
import {
  CreateUserExchangeAccountDto,
  UserExchangeAccountDto,
} from './models/user-exchange-account.dto';
import { UserExchangeAccount } from './models/user-exchange-account.entity';
import { UserExchangeAccountsService } from './user-exchange-accounts.service';

@ApiBearerAuth()
@ApiTags(ApiTag.Accounts)
/**
 * path set via `app.routes`:
 *
 * `api/users/:userId/accounts`
 */
@Controller()
export class UserExchangeAccountsController {
  constructor(
    private readonly _userExchangeAccountsService: UserExchangeAccountsService,
  ) {}

  @UseGuards(AuthGuard('jwt'))
  @Get()
  @ApiOperation({ summary: 'list bots' })
  @ApiOkResponse({
    type: UserExchangeAccountDto,
    isArray: true,
  })
  @ApiForbiddenResponse()
  async findAll(@Param('userId') userId: string) {
    return this._userExchangeAccountsService.findAll(userId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get(':id')
  @ApiOperation({ summary: 'find bot' })
  @ApiOkResponse({
    type: UserExchangeAccountDto,
  })
  @ApiForbiddenResponse()
  async findOne(
    @Param('userId') userId: string,
    @Param('id') id: string,
  ): Promise<UserExchangeAccount> {
    const foundRecord = await this._userExchangeAccountsService.findOne(
      id,
      userId,
    );

    if (!foundRecord) {
      throw new NotFoundException("This Post doesn't exist");
    }

    return foundRecord;
  }

  @UseGuards(AuthGuard('jwt'))
  @Post()
  @ApiOperation({ summary: 'create bot' })
  @ApiCreatedResponse({ type: UserExchangeAccountDto })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse({ description: 'Not found' })
  @ApiConflictResponse()
  async create(
    @Param('userId') userId: string,
    @Body() account: CreateUserExchangeAccountDto,
  ): Promise<UserExchangeAccount> {
    return this._userExchangeAccountsService.create(account, userId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Put(':id')
  @ApiOperation({ summary: 'update bot' })
  @ApiOkResponse({
    type: UserExchangeAccountDto,
  })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse({ description: 'Not found' })
  @ApiConflictResponse()
  async update(
    @Param('userId') userId: string,
    @Param('id') id: string,
    @Body() bot: UserExchangeAccountDto,
  ): Promise<UserExchangeAccount> {
    const { numberOfAffectedRows, updatedAccount } =
      await this._userExchangeAccountsService.update(id, userId, bot);

    if (numberOfAffectedRows === 0) {
      throw new NotFoundException("Bot doesn't exist");
    }

    return updatedAccount;
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  @ApiOperation({ summary: 'delete bot' })
  @ApiNoContentResponse()
  @ApiBadRequestResponse()
  @ApiNotFoundResponse({ description: 'Not found' })
  @HttpCode(204)
  async remove(@Param('userId') userId: string, @Param('id') id: string) {
    const deleted = await this._userExchangeAccountsService.delete(id, userId);

    if (deleted === 0) {
      throw new NotFoundException("Bot doesn't exist");
    }

    return 'Successfully deleted';
  }
}

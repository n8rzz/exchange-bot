import { Module } from '@nestjs/common';
import { UserExchangeAccountsService } from './user-exchange-accounts.service';
import { UserExchangeAccountsController } from './user-exchange-accounts.controller';
import { userExchangeAccountsProviders } from './user-exchange-accounts.provider';

@Module({
  providers: [UserExchangeAccountsService, ...userExchangeAccountsProviders],
  controllers: [UserExchangeAccountsController],
})
export class UserExchangeAccountsModule {}

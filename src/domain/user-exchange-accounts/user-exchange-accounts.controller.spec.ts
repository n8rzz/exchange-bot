import { Test, TestingModule } from '@nestjs/testing';
import { UserExchangeAccountsController } from './user-exchange-accounts.controller';
import { UserExchangeAccountsService } from './user-exchange-accounts.service';

describe('UserExchangeAccountsController', () => {
  let controller: UserExchangeAccountsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserExchangeAccountsController],
      providers: [
        {
          provide: UserExchangeAccountsService,
          useValue: jest.fn(),
        },
      ],
    }).compile();

    controller = module.get<UserExchangeAccountsController>(
      UserExchangeAccountsController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

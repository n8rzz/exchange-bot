import { Test, TestingModule } from '@nestjs/testing';
import { Repository } from '../../constants/repository.constants';
import { UserExchangeAccountsService } from './user-exchange-accounts.service';

describe('UserExchangeAccountsService', () => {
  let service: UserExchangeAccountsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserExchangeAccountsService,
        {
          provide: Repository.UserExchangeAccount,
          useValue: jest.fn(),
        },
      ],
    }).compile();

    service = module.get<UserExchangeAccountsService>(
      UserExchangeAccountsService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

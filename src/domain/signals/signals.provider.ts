import { Repository } from '../../constants/repository.constants';
import { Signal } from './models/signal.entity';

export const signalsProviders = [
  {
    provide: Repository.Signal,
    useValue: Signal,
  },
];

import {
  BelongsTo,
  Column,
  DataType,
  ForeignKey,
  Model,
  Table,
} from 'sequelize-typescript';
import { Bot } from '../../bots/models/bot.entity';
import { Exchange } from '../../exchanges/models/exchange.entity';
import { StrategySegment } from '../../strategy-segments/models/strategy-segment.entity';
import { StrategySegmentType } from '../../strategy-segments/strategy-segments.constants';
import { User } from '../../users/models/user.entity';
import { SignalDto } from './signal.dto';

@Table
export class Signal extends Model<SignalDto> {
  @Column({
    type: DataType.UUID,
    allowNull: false,
    autoIncrement: false,
    defaultValue: DataType.UUIDV4,
    primaryKey: true,
  })
  id: string;

  @Column({
    type: DataType.DATE,
    allowNull: false,
  })
  analysisTime: Date;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  indicatorName: string;

  @Column({
    type: DataType.BOOLEAN,
    allowNull: false,
  })
  isRequired: boolean;

  @Column({
    type: DataType.JSON,
    allowNull: false,
  })
  params: any;

  @Column({
    type: DataType.ENUM({
      values: [...Object.values(StrategySegmentType)],
    }),
    allowNull: false,
  })
  signalResult: StrategySegmentType;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  ticker: string;

  @ForeignKey(() => StrategySegment)
  @Column({
    type: DataType.UUID,
    allowNull: true,
  })
  strategySegmentId: string;

  @BelongsTo(() => StrategySegment)
  strategySegment: StrategySegment;

  @ForeignKey(() => Bot)
  @Column({
    type: DataType.UUID,
    allowNull: true,
  })
  botId: string;

  @BelongsTo(() => Bot)
  bot: Bot;

  @ForeignKey(() => Exchange)
  @Column({
    type: DataType.UUID,
    allowNull: true,
  })
  exchangeId: string;

  @ForeignKey(() => User)
  @Column({
    type: DataType.UUID,
    allowNull: true,
  })
  userId: string;

  @BelongsTo(() => User)
  user: User;
}

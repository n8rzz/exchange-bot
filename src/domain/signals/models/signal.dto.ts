import {
  IsNotEmpty,
  IsEnum,
  IsString,
  IsOptional,
  IsBoolean,
  IsUUID,
} from 'class-validator';
import { StrategySegmentType } from '../../strategy-segments/strategy-segments.constants';

export class CreateSignalDto {
  @IsString()
  @IsNotEmpty()
  readonly analysisTime: Date;

  @IsUUID()
  @IsOptional()
  readonly botId?: string;

  @IsString()
  @IsNotEmpty()
  readonly indicatorName: string;

  @IsBoolean()
  @IsNotEmpty()
  readonly isRequired: boolean;

  @IsNotEmpty()
  params: any;

  @IsEnum(StrategySegmentType)
  @IsNotEmpty()
  readonly signalResult: StrategySegmentType;

  @IsUUID()
  @IsNotEmpty()
  readonly exchangeId: string;

  @IsUUID()
  @IsNotEmpty()
  readonly strategySegmentId: string;

  @IsString()
  @IsNotEmpty()
  readonly ticker: string;

  @IsUUID()
  @IsNotEmpty()
  readonly userId: string;
}

export class SignalDto extends CreateSignalDto {
  @IsUUID()
  @IsNotEmpty()
  readonly id?: string;
}

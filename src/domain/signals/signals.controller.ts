import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Param,
  Body,
  NotFoundException,
  UseGuards,
  HttpCode,
  Query,
  Request,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBearerAuth,
  ApiTags,
  ApiOkResponse,
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiBadRequestResponse,
  ApiConflictResponse,
} from '@nestjs/swagger';
import { ApiTag } from '../../config/swagger.constants';
import { CreateSignalDto, SignalDto } from './models/signal.dto';
import { Signal } from './models/signal.entity';
import { SignalsService } from './signals.service';

@ApiBearerAuth()
@ApiTags(ApiTag.Signals)
/**
 * path set via `app.routes`:
 *
 * `api/bots/:botId/signals`
 */
@Controller()
export class SignalsController {
  constructor(private readonly _signalService: SignalsService) {}

  @Get()
  @ApiOkResponse({
    type: SignalDto,
    isArray: true,
  })
  async findAll(
    @Param('botId') botId: string,
    @Query('ticker') ticker: string,
  ): Promise<Signal[]> {
    return this._signalService.findAll(botId, ticker);
  }

  @Get('recent')
  @ApiOkResponse({
    type: SignalDto,
    isArray: true,
  })
  async recent(
    @Param('botId') botId: string,
    @Query('ticker') ticker: string,
  ): Promise<Signal[]> {
    if (!ticker) {
      throw new NotFoundException('No `ticker` parameter received');
    }

    return this._signalService.findRecentSignalsForTicker(botId, ticker);
  }

  @Get(':id')
  @ApiOkResponse({
    type: SignalDto,
  })
  @ApiNotFoundResponse()
  async findOne(
    @Param('botId') botId: string,
    @Param('id') id: string,
  ): Promise<Signal> {
    const post = await this._signalService.findOne(botId, id);

    if (!post) {
      throw new NotFoundException("This Post doesn't exist");
    }

    return post;
  }

  @UseGuards(AuthGuard('jwt'))
  @Post()
  @ApiCreatedResponse({
    type: SignalDto,
  })
  @ApiBadRequestResponse()
  async create(
    @Param('botId') botId: string,
    @Body() signal: CreateSignalDto,
  ): Promise<Signal> {
    return this._signalService.create(botId, signal);
  }

  @UseGuards(AuthGuard('jwt'))
  @Put(':id')
  @ApiOkResponse({
    type: SignalDto,
  })
  @ApiNotFoundResponse()
  @ApiBadRequestResponse()
  @ApiConflictResponse()
  async update(
    @Param('botId') botId: string,
    @Param('id') id: string,
    @Body() signal: Omit<SignalDto, 'id' | 'botId'>,
  ): Promise<Signal> {
    const { numberOfAffectedRows, updatedSignal } =
      await this._signalService.update(botId, id, signal);

    if (numberOfAffectedRows === 0) {
      throw new NotFoundException("Bot doesn't exist");
    }

    return updatedSignal;
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  @ApiNoContentResponse()
  @ApiNotFoundResponse()
  @HttpCode(204)
  async remove(
    @Param('botId') botId: string,
    @Param('id') id: string,
    @Request() req,
  ) {
    const deleted = await this._signalService.delete(botId, id, req.user.id);

    if (deleted === 0) {
      throw new NotFoundException('Signal does not exist');
    }

    return 'Successfully deleted';
  }
}

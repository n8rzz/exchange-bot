import { Injectable, Inject } from '@nestjs/common';
import { Repository } from '../../constants/repository.constants';
import { StrategySegment } from '../strategy-segments/models/strategy-segment.entity';
import { CreateSignalDto, SignalDto } from './models/signal.dto';
import { Signal } from './models/signal.entity';

@Injectable()
export class SignalsService {
  constructor(
    @Inject(Repository.Signal)
    private readonly _signalRepository: typeof Signal,
  ) {}

  async create(botId: string, signal: CreateSignalDto): Promise<Signal> {
    return this._signalRepository.create<Signal>({
      ...signal,
      botId: botId,
    });
  }

  async findAll(botId: string, ticker?: string): Promise<Signal[]> {
    if (ticker) {
      return this._findAllForTicker(botId, ticker);
    }

    return this._signalRepository.findAll<Signal>({
      where: { botId },
      attributes: { exclude: ['strategyId', 'userId'] },
      include: [
        {
          model: StrategySegment,
          attributes: {
            exclude: ['createdAt', 'userId'],
          },
          order: ['modifiedAt', 'DESC'],
        },
      ],
    });
  }

  /**
   * Given a `botId` and `ticker`, will return only the most recent
   * `signal` for each bot `strategySegment`
   *
   * @param botId {string}
   * @param ticker {string}
   * @returns {Signal[]}
   */
  async findRecentSignalsForTicker(
    botId: string,
    ticker: string,
  ): Promise<Signal[]> {
    const signals = await this._findAllForTicker(botId, ticker);

    // TODO: this could be improved
    return Object.values(
      signals.reduce((sum: { [key: string]: Signal }, v) => {
        if (sum[v.strategySegmentId] != null) {
          const possiblyMostRecent = new Date(
            sum[v.strategySegmentId].analysisTime,
          );
          const currentRecord = new Date(v.analysisTime);

          if (possiblyMostRecent < currentRecord) {
            sum[v.strategySegmentId] = v.toJSON() as Signal;
          }

          return sum;
        }

        sum[v.strategySegmentId] = v.toJSON() as Signal;

        return sum;
      }, {}),
    );
  }

  async findOne(botId: string, id: string): Promise<Signal> {
    return this._signalRepository.findOne<Signal>({
      where: { botId, id },
      attributes: { exclude: ['userId'] },
    });
  }

  async delete(botId: string, id: string, userId: string): Promise<number> {
    return this._signalRepository.destroy<Signal>({
      where: { botId, id, userId },
    });
  }

  async update(botId: string, id: string, data: Partial<SignalDto>) {
    const [numberOfAffectedRows, [updatedSignal]] =
      await this._signalRepository.update<Signal>(
        {
          ...data,
          botId,
          id,
        },
        { where: { botId, id, userId: data.userId }, returning: true },
      );

    return { numberOfAffectedRows, updatedSignal };
  }

  private async _findAllForTicker(
    botId: string,
    ticker: string,
  ): Promise<Signal[]> {
    return this._signalRepository.findAll<Signal>({
      where: { botId, ticker },
      attributes: { exclude: ['strategyId', 'userId'] },
    });
  }
}

import { Module } from '@nestjs/common';
import { SignalsService } from './signals.service';
import { SignalsController } from './signals.controller';
import { signalsProviders } from './signals.provider';

@Module({
  providers: [SignalsService, ...signalsProviders],
  controllers: [SignalsController],
  exports: [SignalsService],
})
export class SignalsModule {}

import { Injectable, Inject } from '@nestjs/common';
import { Repository } from '../../constants/repository.constants';
import { User } from '../users/models/user.entity';
import {
  CreateStrategySegmentDto,
  StrategySegmentDto,
} from './models/strategy-segment.dto';
import { StrategySegment } from './models/strategy-segment.entity';

@Injectable()
export class StrategySegmentsService {
  constructor(
    @Inject(Repository.StrategySegment)
    private readonly strategySegmentRepository: typeof StrategySegment,
  ) {}

  async create(
    strategyId: string,
    strategySegmentToCreate: CreateStrategySegmentDto,
  ): Promise<StrategySegment> {
    return this.strategySegmentRepository.create<StrategySegment>({
      ...strategySegmentToCreate,
      strategyId: strategyId,
    });
  }

  async findAll(): Promise<StrategySegment[]> {
    return this.strategySegmentRepository.findAll<StrategySegment>({
      include: [{ model: User, attributes: { exclude: ['password'] } }],
    });
  }

  async findOne(strategyId: string, id: string): Promise<StrategySegment> {
    return this.strategySegmentRepository.findOne({
      where: {
        id,
        strategyId,
      },
      include: [
        {
          model: User,
          attributes: { exclude: ['createdAt', 'modifiedAt', 'password'] },
        },
      ],
    });
  }

  async delete(strategyId: string, id: string, userId: string) {
    return this.strategySegmentRepository.destroy({
      where: { id, strategyId, userId },
    });
  }

  async update(
    strategyId: string,
    id: string,
    data: Partial<StrategySegmentDto>,
  ) {
    const [numberOfAffectedRows, [updatedStrategy]] =
      await this.strategySegmentRepository.update(
        {
          ...data,
          strategyId,
        },
        { where: { id, strategyId, userId: data.userId }, returning: true },
      );

    return { numberOfAffectedRows, updatedStrategy };
  }
}

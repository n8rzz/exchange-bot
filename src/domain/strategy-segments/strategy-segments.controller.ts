import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Param,
  Body,
  NotFoundException,
  UseGuards,
  Request,
  HttpCode,
  UsePipes,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  CreateStrategySegmentDto,
  StrategySegmentDto,
} from './models/strategy-segment.dto';
import {
  ApiBearerAuth,
  ApiTags,
  ApiOperation,
  ApiOkResponse,
  ApiForbiddenResponse,
  ApiCreatedResponse,
  ApiBadRequestResponse,
  ApiNotFoundResponse,
  ApiConflictResponse,
  ApiNoContentResponse,
} from '@nestjs/swagger';
import { StrategySegment } from './models/strategy-segment.entity';
import { StrategySegmentParamValidationPipe } from './strategy-segments-param-validation.pipe';
import { StrategySegmentsService } from './strategy-segments.service';
import { segmentParmsSchemaFactory } from './strategy-segments.utils';
import { ApiTag } from '../../config/swagger.constants';

@ApiBearerAuth()
@ApiTags(ApiTag.StrategySegments)
/**
 * path set via `app.routes`:
 *
 * `api/strategy/:strategyId/segments`
 */
@Controller()
export class StrategySegmentsController {
  constructor(
    private readonly strategySegmentsService: StrategySegmentsService,
  ) {}

  @Get()
  @ApiOperation({ summary: 'list strategy-segments' })
  @ApiOkResponse({
    type: StrategySegmentDto,
    isArray: true,
  })
  @ApiForbiddenResponse()
  async findAll() {
    return this.strategySegmentsService.findAll();
  }

  @Get(':id')
  @ApiOperation({ summary: 'find strategy-segment' })
  @ApiOkResponse({
    type: StrategySegmentDto,
  })
  @ApiForbiddenResponse()
  async findOne(
    @Param('strategyId') strategyId: string,
    @Param('id') id: string,
  ): Promise<StrategySegment> {
    const post = await this.strategySegmentsService.findOne(strategyId, id);

    if (!post) {
      throw new NotFoundException("This Post doesn't exist");
    }

    return post;
  }

  @UseGuards(AuthGuard('jwt'))
  @Post()
  @ApiOperation({ summary: 'create strategy-segment' })
  @ApiCreatedResponse({ type: StrategySegmentDto })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse({ description: 'Not found' })
  @ApiConflictResponse()
  @UsePipes(new StrategySegmentParamValidationPipe(segmentParmsSchemaFactory))
  async create(
    @Param('strategyId') strategyId: string,
    @Body() strategySegment: CreateStrategySegmentDto,
  ): Promise<StrategySegment> {
    return this.strategySegmentsService.create(strategyId, strategySegment);
  }

  @UseGuards(AuthGuard('jwt'))
  @Put(':id')
  @ApiOperation({ summary: 'update strategy-segment' })
  @ApiOkResponse({
    type: StrategySegmentDto,
  })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse({ description: 'Not found' })
  @ApiConflictResponse()
  @UsePipes(new StrategySegmentParamValidationPipe(segmentParmsSchemaFactory))
  async update(
    @Param('strategyId') strategyId: string,
    @Param('id') id: string,
    @Body() strategySegment: StrategySegmentDto,
  ): Promise<StrategySegment> {
    const { numberOfAffectedRows, updatedStrategy } =
      await this.strategySegmentsService.update(
        strategyId,
        id,
        strategySegment,
      );

    if (numberOfAffectedRows === 0) {
      throw new NotFoundException("Strategy doesn't exist");
    }

    return updatedStrategy;
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  @ApiOperation({ summary: 'delete strategy-segment' })
  @ApiNoContentResponse()
  @ApiBadRequestResponse()
  @ApiNotFoundResponse({ description: 'Not found' })
  @HttpCode(204)
  async remove(
    @Param('strategyId') strategyId: string,
    @Param('id') id: string,
    @Request() req,
  ) {
    const deleted = await this.strategySegmentsService.delete(
      strategyId,
      id,
      req.user.id,
    );

    if (deleted === 0) {
      throw new NotFoundException("Strategy doesn't exist");
    }

    return 'Successfully deleted';
  }
}

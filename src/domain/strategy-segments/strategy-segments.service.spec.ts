import { Test, TestingModule } from '@nestjs/testing';
import { Repository } from '../../constants/repository.constants';
import { StrategySegmentsService } from './strategy-segments.service';

describe('StrategySegmentsService', () => {
  let service: StrategySegmentsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        StrategySegmentsService,
        {
          provide: Repository.StrategySegment,
          useValue: jest.fn(),
        },
      ],
    }).compile();

    service = module.get<StrategySegmentsService>(StrategySegmentsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

export enum IndicatorName {
  Apo = 'Absolute Price Oscillator',
  Ema = 'Exponential Moving Average',
  Sma = 'Simple Moving Average',
  Rsi = 'Relative Strength Indicator',
}

export enum ComparisonSymbol {
  EqualTo = '=',
  GreaterThan = '>',
  GreaterThanOrEqualTo = '>=',
  LessThan = '<',
  LessThanOrEqualTo = '<=',
}

export enum MovingAverageCrossingType {
  FastAboveSlow = 'FastAboveSlow',
  FastBelowSlow = 'FastBelowSlow',
}

export enum MovingAverageType {
  Exponential = 'Exponential',
  Simple = 'Simple',
}

export enum StrategySegmentType {
  Buy = 'Buy',
  BuyToCover = 'BuyToCover',
  None = 'None',
  Sell = 'Sell',
  SellToCover = 'SellToCover',
}

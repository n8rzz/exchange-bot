import * as Joi from '@hapi/joi';
import { StrategySegment } from './models/strategy-segment.entity';
import {
  ComparisonSymbol,
  IndicatorName,
  MovingAverageCrossingType,
  MovingAverageType,
} from './strategy-segments.constants';

const movingAverageValidationSchema: Joi.ObjectSchema = Joi.object().keys({
  crossingType: Joi.string()
    .valid(...Object.values(MovingAverageCrossingType))
    .required(),
  fastPeriod: Joi.number().min(1).max(200).required(),
  movingAverageType: Joi.string()
    .valid(...Object.values(MovingAverageType))
    .required(),
  slowPeriod: Joi.number().min(1).max(200).required(),
});

const rsiValidationSchema: Joi.ObjectSchema = Joi.object().keys({
  period: Joi.number().min(1).max(200).required(),
  signalWhen: Joi.object()
    .keys({
      comparison: Joi.string()
        .valid(...Object.values(ComparisonSymbol))
        .required(),
      value: Joi.number().required(),
    })
    .required(),
});

const segmentTypeSchemaMap: Record<IndicatorName, Joi.ObjectSchema> = {
  [IndicatorName.Apo]: movingAverageValidationSchema,
  [IndicatorName.Ema]: movingAverageValidationSchema,
  [IndicatorName.Sma]: movingAverageValidationSchema,
  [IndicatorName.Rsi]: rsiValidationSchema,
};

export const compareCalculatationMap: Record<
  ComparisonSymbol,
  (value: number, comparison: number) => boolean
> = {
  [ComparisonSymbol.GreaterThanOrEqualTo]: (
    value: number,
    comparison: number,
  ) => value >= comparison,
  [ComparisonSymbol.GreaterThan]: (value: number, comparison: number) =>
    value > comparison,
  [ComparisonSymbol.EqualTo]: (value: number, comparison: number) =>
    value === comparison,
  [ComparisonSymbol.LessThanOrEqualTo]: (value: number, comparison: number) =>
    value <= comparison,
  [ComparisonSymbol.LessThan]: (value: number, comparison: number) =>
    value < comparison,
};

export const compareCrossingTypeMap: Record<
  MovingAverageCrossingType,
  (fast: number, slow: number) => boolean
> = {
  [MovingAverageCrossingType.FastAboveSlow]: (fast: number, slow: number) =>
    fast > slow,
  [MovingAverageCrossingType.FastBelowSlow]: (fast: number, slow: number) =>
    fast < slow,
};

export const segmentParmsSchemaFactory = (
  type: IndicatorName,
): Joi.ObjectSchema => {
  return segmentTypeSchemaMap[type];
};

export const findSegmentFromIndicatorAndPeriod = (
  segments: StrategySegment[],
  indicator: IndicatorName,
  period: number,
): StrategySegment => {
  return segments.find(
    (segment: StrategySegment) =>
      segment.indicator === indicator && segment.period === period,
  );
};

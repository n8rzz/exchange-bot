import {
  PipeTransform,
  Injectable,
  BadRequestException,
  ArgumentMetadata,
} from '@nestjs/common';
import { ObjectSchema } from '@hapi/joi';
import { IndicatorName } from './strategy-segments.constants';
import { IStrategySegment } from './strategy-segments.types';

@Injectable()
export class StrategySegmentParamValidationPipe implements PipeTransform {
  constructor(private schemaFactory: (value: IndicatorName) => ObjectSchema) {}

  transform(value: IStrategySegment, metadata: ArgumentMetadata) {
    if (metadata.type !== 'body') {
      return value;
    }

    const schema = this.schemaFactory(value.indicator);
    const { error } = schema.validate(value.params, { abortEarly: false });

    if (error) {
      throw new BadRequestException(error.details.map((e) => e.message));
    }

    return value;
  }
}

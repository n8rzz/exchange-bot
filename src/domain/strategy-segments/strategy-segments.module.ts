import { Module } from '@nestjs/common';
import { StrategySegmentsService } from './strategy-segments.service';
import { StrategySegmentsController } from './strategy-segments.controller';
import { strategySegmentsProviders } from './strategy-segments.provider';

@Module({
  providers: [StrategySegmentsService, ...strategySegmentsProviders],
  controllers: [StrategySegmentsController],
})
export class StrategySegmentsModule {}

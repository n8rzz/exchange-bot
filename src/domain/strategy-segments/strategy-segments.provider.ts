import { Repository } from '../../constants/repository.constants';
import { StrategySegment } from './models/strategy-segment.entity';

export const strategySegmentsProviders = [
  {
    provide: Repository.StrategySegment,
    useValue: StrategySegment,
  },
];

import { Test, TestingModule } from '@nestjs/testing';
import { StrategySegmentsController } from './strategy-segments.controller';
import { StrategySegmentsService } from './strategy-segments.service';

describe('StrategySegmentsController', () => {
  let controller: StrategySegmentsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: StrategySegmentsService,
          useValue: jest.fn(),
        },
      ],
      controllers: [StrategySegmentsController],
    }).compile();

    controller = module.get<StrategySegmentsController>(
      StrategySegmentsController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

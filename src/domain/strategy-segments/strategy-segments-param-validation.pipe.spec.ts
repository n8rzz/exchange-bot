import { StrategySegmentParamValidationPipe } from './strategy-segments-param-validation.pipe';
import { segmentParmsSchemaFactory } from './strategy-segments.utils';

describe('StrategySegmentParamValidationPipe', () => {
  it('should be defined', () => {
    expect(
      new StrategySegmentParamValidationPipe(segmentParmsSchemaFactory),
    ).toBeDefined();
  });
});

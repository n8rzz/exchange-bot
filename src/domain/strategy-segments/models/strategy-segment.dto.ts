import { IsUUID } from 'class-validator';
import {
  IndicatorName,
  StrategySegmentType,
} from '../strategy-segments.constants';
import { IStrategySegment } from '../strategy-segments.types';

// TODO: add class-validators
export class CreateStrategySegmentDto implements Omit<IStrategySegment, 'id'> {
  indicator: IndicatorName;
  isRequired: boolean;
  keepSignalLength: number;
  params: any;
  period: number;
  type: StrategySegmentType;

  @IsUUID()
  strategyId: string;

  @IsUUID()
  userId: string;
}

export class StrategySegmentDto extends CreateStrategySegmentDto {
  @IsUUID()
  readonly id?: string;
}

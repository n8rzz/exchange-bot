import {
  Table,
  Column,
  Model,
  DataType,
  ForeignKey,
  BelongsTo,
  HasMany,
} from 'sequelize-typescript';
import { Signal } from '../../signals/models/signal.entity';
import { Strategy } from '../../strategies/models/strategy.entity';
import { User } from '../../users/models/user.entity';
import {
  IndicatorName,
  StrategySegmentType,
} from '../strategy-segments.constants';
import { StrategySegmentParams } from '../strategy-segments.types';
import { StrategySegmentDto } from './strategy-segment.dto';

@Table
export class StrategySegment extends Model<StrategySegmentDto> {
  @Column({
    type: DataType.UUID,
    allowNull: false,
    autoIncrement: false,
    defaultValue: DataType.UUIDV4,
    primaryKey: true,
  })
  id: string;

  @Column({
    type: DataType.ENUM({
      values: [...Object.values(IndicatorName)],
    }),
    allowNull: false,
  })
  indicator: IndicatorName;

  @Column({
    type: DataType.BOOLEAN,
    defaultValue: true,
  })
  isRequired: boolean;

  @Column({
    type: DataType.INTEGER,
    defaultValue: 1,
    allowNull: false,
  })
  keepSignalLength: number;

  @Column({
    type: DataType.JSON,
    allowNull: false,
  })
  params: StrategySegmentParams;

  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  period: number;

  @Column({
    type: DataType.ENUM({
      values: [...Object.keys(StrategySegmentType)],
    }),
    allowNull: false,
  })
  type: StrategySegmentType;

  @ForeignKey(() => Strategy)
  @Column({
    type: DataType.UUID,
    allowNull: false,
  })
  strategyId: string;

  @BelongsTo(() => Strategy)
  strategy: Strategy;

  @ForeignKey(() => User)
  @Column({
    type: DataType.UUID,
    allowNull: false,
  })
  userId: string;

  @BelongsTo(() => User)
  user: Strategy;

  @HasMany(() => Signal)
  signals: Signal;
}

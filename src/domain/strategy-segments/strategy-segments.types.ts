/* eslint-disable @typescript-eslint/no-empty-interface */
import {
  ComparisonSymbol,
  IndicatorName,
  MovingAverageCrossingType,
  MovingAverageType,
  StrategySegmentType,
} from './strategy-segments.constants';

export interface ISignalWhen {
  comparison: ComparisonSymbol;
  value: number;
}

export interface IMovingAverageParams {
  crossingType: MovingAverageCrossingType;
  fastPeriod: number;
  movingAverageType: MovingAverageType;
  slowPeriod: number;
}

export interface IApoParams extends IMovingAverageParams {}

export interface IEmaParams extends IMovingAverageParams {}

export interface ISmaParams extends IMovingAverageParams {}

export interface IRsiParams {
  period: number;
  signalWhen: ISignalWhen;
}

export type StrategySegmentParams =
  | IApoParams
  | IEmaParams
  | ISmaParams
  | IRsiParams;

export interface IStrategySegment<T = StrategySegmentParams> {
  id: string;
  isRequired: boolean;
  // number of candles
  keepSignalLength: number;
  indicator: IndicatorName;
  params: T;
  period: number;
  type: StrategySegmentType;
}

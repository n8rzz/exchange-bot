import {
  IsEmpty,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsString,
  IsUUID,
} from 'class-validator';
import { OrderSide, OrderType } from '../orders.constants';
import { ICreateOrderRequest } from '../orders.types';

export class CreateOrderDto implements ICreateOrderRequest {
  @IsNumber()
  @IsNotEmpty()
  amount: number;

  @IsNumber()
  @IsNotEmpty()
  price: number;

  @IsEnum(OrderSide)
  @IsNotEmpty()
  side: OrderSide;

  @IsString()
  @IsNotEmpty()
  symbol: string;

  @IsEnum(OrderType)
  @IsNotEmpty()
  type: OrderType;

  @IsUUID()
  @IsNotEmpty()
  botId: string;

  @IsUUID()
  @IsNotEmpty()
  exchangeId: string;

  @IsEmpty()
  userId: string;
}

export class OrderDto extends CreateOrderDto {
  id?: string;
}

// TODO: This may become a future `Trades` entity
// export class OrderDto implements IOrder {
//   amount: number;
//   average?: number;
//   botId: string;
//   clientOrderId?: string;
//   cost: number;
//   datetime: string;
//   fee?: IOrderFeeResponse;
//   filled: number;
//   id: string;
//   info: unknown[];
//   lastTradeTimestamp: number;
//   price: number;
//   remaining: number;
//   side: OrderSide;
//   status: OrderStatus;
//   symbol: string;
//   timeInForce: OrderTimeInForce;
//   timestamp: number;
//   trades: unknown[];
//   type: OrderType;
//   exchangeId: string;
//   userId: string;
// }

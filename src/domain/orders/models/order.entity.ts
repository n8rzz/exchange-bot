import {
  Column,
  DataType,
  ForeignKey,
  Table,
  Model,
} from 'sequelize-typescript';
import { Bot } from '../../bots/models/bot.entity';
import { Exchange } from '../../exchanges/models/exchange.entity';
import { User } from '../../users/models/user.entity';
import { OrderSide, OrderType } from '../orders.constants';
import { OrderDto } from './order.dto';

@Table
export class Order extends Model<OrderDto> {
  @Column({
    type: DataType.UUID,
    allowNull: false,
    autoIncrement: false,
    defaultValue: DataType.UUIDV4,
    primaryKey: true,
  })
  id: string;

  @Column({
    type: DataType.NUMBER,
    allowNull: false,
  })
  amount: number;

  @Column({
    type: DataType.NUMBER,
    allowNull: false,
  })
  price: number;

  @Column({
    type: DataType.ENUM({
      values: Object.values(OrderSide),
    }),
    allowNull: false,
  })
  side: OrderSide;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  symbol: string;

  @Column({
    type: DataType.ENUM({
      values: Object.values(OrderType),
    }),
    allowNull: false,
  })
  type: OrderType;

  @ForeignKey(() => Bot)
  @Column({
    type: DataType.UUID,
    allowNull: false,
  })
  botId: string;

  @ForeignKey(() => Exchange)
  @Column({
    type: DataType.UUID,
    allowNull: false,
  })
  exchangeId: string;

  @ForeignKey(() => User)
  @Column({
    type: DataType.UUID,
    allowNull: false,
  })
  userId: string;
}

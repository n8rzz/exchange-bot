import { Module } from '@nestjs/common';
import { OrdersController } from './orders.controller';
import { ordersProviders } from './orders.providers';
import { OrdersService } from './orders.service';

@Module({
  controllers: [OrdersController],
  providers: [OrdersService, ...ordersProviders],
})
export class OrdersModule {}

import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpException,
  HttpStatus,
  NotFoundException,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBearerAuth,
  ApiTags,
  ApiOperation,
  ApiOkResponse,
  ApiBadRequestResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiCreatedResponse,
  ApiConflictResponse,
  ApiForbiddenResponse,
  ApiNotImplementedResponse,
} from '@nestjs/swagger';
import { ApiTag } from '../../config/swagger.constants';
import { CreateOrderDto, OrderDto } from './models/order.dto';
import { Order } from './models/order.entity';
import { OrdersService } from './orders.service';

@ApiBearerAuth()
@ApiTags(ApiTag.Orders)
/**
 * path set via `app.routes`:
 *
 * `api/users/:userId/orders`
 */
@Controller()
export class OrdersController {
  constructor(private readonly _ordersService: OrdersService) {}

  @UseGuards(AuthGuard('jwt'))
  @Get()
  @ApiOperation({ summary: 'list exchanges' })
  @ApiOkResponse({
    type: OrderDto,
    isArray: true,
  })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  async findAll(@Param('userId') userId: string) {
    return this._ordersService.findAll(userId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get(':id')
  @ApiOperation({ summary: 'find order' })
  @ApiOkResponse({
    type: OrderDto,
  })
  async findOne(
    @Param('userId') userId: string,
    @Param('id') id: string,
  ): Promise<Order> {
    const post = await this._ordersService.findOne(id, userId);

    if (!post) {
      throw new NotFoundException("This Order doesn't exist");
    }

    return post;
  }

  @UseGuards(AuthGuard('jwt'))
  @Post()
  @ApiOperation({ summary: 'create order' })
  @ApiCreatedResponse({
    type: OrderDto,
  })
  @ApiBadRequestResponse({ description: 'Bad request' })
  @ApiNotFoundResponse({ description: 'Not found' })
  @ApiConflictResponse({ description: 'Conflict' })
  async create(
    @Param('userId') userId: string,
    @Body() exchange: CreateOrderDto,
  ): Promise<Order> {
    return this._ordersService.create(exchange, userId);
  }

  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'update order' })
  @Put(':id')
  @ApiNotImplementedResponse()
  @HttpCode(HttpStatus.NOT_IMPLEMENTED)
  async update(): Promise<void> {
    throw new HttpException('Forbidden', HttpStatus.NOT_IMPLEMENTED);
  }

  @UseGuards(AuthGuard('jwt'))
  @ApiOperation({ summary: 'delete order' })
  @Delete(':id')
  @ApiNoContentResponse({
    description: 'Empty response',
  })
  @ApiBadRequestResponse({ description: 'Bad request' })
  @ApiNotFoundResponse({ description: 'Not found' })
  @HttpCode(204)
  async remove(
    @Param('userId') userId: string,
    @Param('id') id: string,
  ): Promise<string> {
    const deleted = await this._ordersService.delete(id, userId);

    if (deleted === 0) {
      throw new NotFoundException("Order doesn't exist");
    }

    return 'Successfully deleted';
  }
}

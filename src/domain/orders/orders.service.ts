import { Inject, Injectable } from '@nestjs/common';
import { Repository } from '../../constants/repository.constants';
import { CreateOrderDto, OrderDto } from './models/order.dto';
import { Order } from './models/order.entity';

export interface IOrdersService {
  findAll: (userId: string) => Promise<Order[]>;
  findOne: (id: string, userId: string) => Promise<Order>;
  create: (order: CreateOrderDto, userId: string) => Promise<Order>;
  update: (
    id: string,
    userId: string,
    orderToUpdate: OrderDto,
  ) => Promise<{ numberOfAffectedRows: number; updatedOrder: Order }>;
  delete: (id: string, userId: string) => Promise<number>;
}

@Injectable()
export class OrdersService implements IOrdersService {
  constructor(
    @Inject(Repository.Order)
    private readonly _orderRepository: typeof Order,
  ) {}

  async create(order: CreateOrderDto, userId: string): Promise<Order> {
    return this._orderRepository.create<Order>({
      ...order,
      userId: userId,
    });
  }

  async findAll(userId: string): Promise<Order[]> {
    return this._orderRepository.findAll<Order>({
      where: { userId },
    });
  }

  async findOne(id: string, userId: string): Promise<Order> {
    return this._orderRepository.findOne({
      where: { id, userId },
    });
  }

  async delete(id: string, userId: string): Promise<number> {
    return this._orderRepository.destroy({ where: { id, userId } });
  }

  async update(id: string, userId: string, orderToUpdate: Partial<OrderDto>) {
    const [numberOfAffectedRows, [updatedOrder]] =
      await this._orderRepository.update(
        { ...orderToUpdate },
        { where: { id, userId }, returning: true },
      );

    return { numberOfAffectedRows, updatedOrder };
  }
}

export enum OrderType {
  Market = 'market',
  Limit = 'limit',
}

export enum OrderSide {
  Buy = 'buy',
  Sell = 'sell',
}

export enum OrderStatus {
  Canceled = 'canceled',
  Closed = 'closed',
  Expired = 'expired',
  Open = 'open',
}

export enum OrderTimeInForce {
  Gtc = 'GTC',
  Ioc = 'IOC',
  Fok = 'FOK',
  Po = 'PO',
}

import {
  OrderSide,
  OrderStatus,
  OrderTimeInForce,
  OrderType,
} from './orders.constants';

export interface IOrderFeeResponse {
  cost?: number;
  currency?: string;
  rate?: number;
}

export interface ICreateOrderRequest {
  amount: number;
  botId: string;
  exchangeId: string;
  price: number;
  side: OrderSide;
  symbol: string;
  type: OrderType;
  userId: string;
}

export interface IOrder extends ICreateOrderRequest {
  average?: number;
  clientOrderId?: string;
  cost: number;
  datetime: string;
  fee?: IOrderFeeResponse;
  filled: number;
  id: string;
  info: unknown[];
  lastTradeTimestamp: number;
  remaining: number;
  status: OrderStatus;
  timeInForce: OrderTimeInForce;
  timestamp: number;
  trades: unknown[];
}

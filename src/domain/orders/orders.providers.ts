import { Repository } from '../../constants/repository.constants';
import { Order } from './models/order.entity';

export const ordersProviders = [
  {
    provide: Repository.Order,
    useValue: Order,
  },
];

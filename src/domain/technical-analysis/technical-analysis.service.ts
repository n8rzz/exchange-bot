import { Injectable } from '@nestjs/common';
import { ema, rsi, sma } from 'technicalindicators';
import { StrategySegment } from '../strategy-segments/models/strategy-segment.entity';
import {
  IndicatorName,
  StrategySegmentType,
} from '../strategy-segments/strategy-segments.constants';
import {
  IApoParams,
  IEmaParams,
  IMovingAverageParams,
  IRsiParams,
  ISignalWhen,
  ISmaParams,
  StrategySegmentParams,
} from '../strategy-segments/strategy-segments.types';
import {
  compareCalculatationMap,
  compareCrossingTypeMap,
  findSegmentFromIndicatorAndPeriod,
} from '../strategy-segments/strategy-segments.utils';
import { ITechnicalAnalysisValues } from './technical-analysis.types';

@Injectable()
export class TechnicalAnalysisService {
  /**
   *
   *
   * @param name {IndicatorName}
   * @param params {StrategySegmentParams}
   * @param values number[]
   * @param keepSignalLength number
   * @returns {ITechnicalAnalysisValues}
   */
  public calculateIndicatorValuesForSegmentParams(
    name: IndicatorName,
    params: StrategySegmentParams,
    values: number[],
    keepSignalLength: number,
  ): Partial<ITechnicalAnalysisValues> {
    switch (name) {
      case IndicatorName.Apo:
        const apoParams = params as IApoParams;
        const apoResult = this.apo(
          apoParams.fastPeriod,
          apoParams.slowPeriod,
          values,
        );

        return {
          result: apoResult.slice(0, keepSignalLength),
        };
      case IndicatorName.Ema:
        const emaParams = params as IEmaParams;
        const emaFastPeriod = this.ema(emaParams.fastPeriod, values);
        const emaSlowPeriod = this.ema(emaParams.slowPeriod, values);

        return {
          fastPeriodResult: emaFastPeriod.slice(0, keepSignalLength),
          slowPeriodResult: emaSlowPeriod.slice(0, keepSignalLength),
        };
      case IndicatorName.Rsi:
        const rsiParams = params as IRsiParams;
        const rsiResult = this.rsi(rsiParams.period, values);

        return {
          result: rsiResult.slice(0, keepSignalLength),
        };
      case IndicatorName.Sma:
        const smaParams = params as ISmaParams;
        const smaFastPeriod = this.sma(smaParams.fastPeriod, values);
        const smaSlowPeriod = this.sma(smaParams.slowPeriod, values);

        return {
          fastPeriodResult: smaFastPeriod.slice(0, keepSignalLength),
          slowPeriodResult: smaSlowPeriod.slice(0, keepSignalLength),
        };
      default:
        break;
    }
  }

  public generateSignalForIndicator(
    closePriceList: number[],
    indicator: IndicatorName,
    period: number,
    strategySegments: StrategySegment[],
  ): Partial<ITechnicalAnalysisValues> {
    const segment = findSegmentFromIndicatorAndPeriod(
      strategySegments,
      indicator,
      period,
    );
    const taResult = this.calculateIndicatorValuesForSegmentParams(
      segment.indicator,
      segment.params,
      closePriceList,
      segment.keepSignalLength,
    );
    const comparisonResult = this.calculateComparisonResult(taResult, segment);
    taResult.strategySegmentId = segment.id;
    taResult.isRequired = segment.isRequired;

    taResult.signal = comparisonResult
      ? segment.type
      : StrategySegmentType.None;

    return taResult;
  }

  public calculateComparisonResult(
    taResult: Partial<ITechnicalAnalysisValues>,
    segment: StrategySegment,
  ): number {
    let comparisonResult = null;

    switch (segment.indicator) {
      case IndicatorName.Rsi:
        comparisonResult = this.calculateSignalWithSignalWhen(
          taResult as Pick<ITechnicalAnalysisValues, 'result'>,
          (segment.params as IRsiParams).signalWhen,
        );
        break;
      case IndicatorName.Apo:
        comparisonResult = taResult.result[0] >= 0;
        break;
      default:
        comparisonResult = compareCrossingTypeMap[
          (segment.params as IMovingAverageParams).crossingType
        ](taResult.fastPeriodResult[0], taResult.slowPeriodResult[0]);
        break;
    }

    return comparisonResult;
  }

  public calculateSignalWithSignalWhen(
    taResult: Pick<ITechnicalAnalysisValues, 'result'>,
    signalWhen: ISignalWhen,
  ) {
    const compareFn = compareCalculatationMap[signalWhen.comparison];

    return taResult.result
      .map((v: number) => compareFn(v, signalWhen.value))
      .every(Boolean);
  }

  public sma(period: number, values: number[]): number[] {
    return sma({
      period: period,
      values: values,
    });
  }

  public ema(period: number, values: number[]): number[] {
    return ema({
      period: period,
      values: values,
    });
  }

  public rsi(period: number, values: number[]): number[] {
    return rsi({
      period: period,
      values: values,
    });
  }

  public apo(fast: number, slow: number, values: number[]): number[] {
    const fastEma = this.ema(fast, values);
    const slowEma = this.ema(slow, values);

    return fastEma.map((v, i) => v - slowEma[i]);
  }
}

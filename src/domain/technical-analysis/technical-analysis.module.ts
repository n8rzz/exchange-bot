import { forwardRef, Module } from '@nestjs/common';
import { QueueModule } from '../../queue/queue.module';
import { TechnicalAnalysisService } from './technical-analysis.service';

@Module({
  imports: [forwardRef(() => QueueModule)],
  providers: [TechnicalAnalysisService],
  exports: [TechnicalAnalysisService],
})
export class TechnicalAnalysisModule {}

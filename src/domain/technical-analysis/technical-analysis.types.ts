import { StrategySegmentType } from '../strategy-segments/strategy-segments.constants';

export interface ITechnicalAnalysisValues {
  fastPeriodResult?: number[];
  isRequired: boolean;
  result: number[];
  signal: StrategySegmentType;
  slowPeriodResult?: number[];
  strategySegmentId: string;
}

import { Test, TestingModule } from '@nestjs/testing';
import { StrategySegment } from '../strategy-segments/models/strategy-segment.entity';
import { IndicatorName } from '../strategy-segments/strategy-segments.constants';
import { TechnicalAnalysisService } from './technical-analysis.service';
import {
  apoParamsMock,
  emaParamsMock,
  ohlcvCloseValueMock,
  rsiParamsMock,
  smaParamsMock,
  strategyMock,
} from './__mocks__/technical-analysis.mocks';

describe('TechnicalAnalysisService', () => {
  let service: TechnicalAnalysisService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TechnicalAnalysisService],
    }).compile();

    service = module.get<TechnicalAnalysisService>(TechnicalAnalysisService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('when passed valid APO params', () => {
    test('should not throw', () => {
      expect(() =>
        service.calculateIndicatorValuesForSegmentParams(
          IndicatorName.Apo,
          apoParamsMock,
          ohlcvCloseValueMock,
          1,
        ),
      ).not.toThrow();
    });

    test('should return an object with key result', () => {
      const { result } = service.calculateIndicatorValuesForSegmentParams(
        IndicatorName.Apo,
        apoParamsMock,
        ohlcvCloseValueMock,
        1,
      );
      const expectedResult = [6.087179487179441];

      expect(result).toEqual(expectedResult);
    });
  });

  describe('when passed valid RSI params', () => {
    test('should not throw', () => {
      expect(() =>
        service.calculateIndicatorValuesForSegmentParams(
          IndicatorName.Rsi,
          rsiParamsMock,
          ohlcvCloseValueMock,
          1,
        ),
      ).not.toThrow();
    });

    test('should return an object with key result', () => {
      const { result } = service.calculateIndicatorValuesForSegmentParams(
        IndicatorName.Rsi,
        rsiParamsMock,
        ohlcvCloseValueMock,
        1,
      );
      const expectedResult = [50];

      expect(result).toEqual(expectedResult);
    });
  });

  describe('when passed valid SMA params', () => {
    test('should not throw', () => {
      expect(() =>
        service.calculateIndicatorValuesForSegmentParams(
          IndicatorName.Sma,
          smaParamsMock,
          ohlcvCloseValueMock,
          1,
        ),
      ).not.toThrow();
    });

    test('should return an object with keys fastPeriodResult and slowPeriodResult', () => {
      const { fastPeriodResult, slowPeriodResult } =
        service.calculateIndicatorValuesForSegmentParams(
          IndicatorName.Sma,
          smaParamsMock,
          ohlcvCloseValueMock,
          1,
        );
      const expectedFastPeriodResult = [177.77777777777777];
      const expectedSlowPeriodResult = [190];

      expect(fastPeriodResult).toEqual(expectedFastPeriodResult);
      expect(slowPeriodResult).toEqual(expectedSlowPeriodResult);
    });
  });

  describe('when passed valid EMA params', () => {
    test('should not throw', () => {
      expect(() =>
        service.calculateIndicatorValuesForSegmentParams(
          IndicatorName.Ema,
          emaParamsMock,
          ohlcvCloseValueMock,
          1,
        ),
      ).not.toThrow();
    });

    test('should return an object with keys fastPeriodResult and slowPeriodResult', () => {
      const { fastPeriodResult, slowPeriodResult } =
        service.calculateIndicatorValuesForSegmentParams(
          IndicatorName.Ema,
          emaParamsMock,
          ohlcvCloseValueMock,
          1,
        );
      const expectedFastPeriodResult = [177.77777777777777];
      const expectedSlowPeriodResult = [190];

      expect(fastPeriodResult.slice(0, 5)).toEqual(expectedFastPeriodResult);
      expect(slowPeriodResult.slice(0, 5)).toEqual(expectedSlowPeriodResult);
    });
  });

  describe('.generateSignalForIndicator()', () => {
    describe('when passed `IndicatorName.Rsi`', () => {
      test('should return object with #result key', () => {
        const result = service.generateSignalForIndicator(
          ohlcvCloseValueMock,
          IndicatorName.Rsi,
          300,
          strategyMock.strategySegments as StrategySegment[],
        );

        expect(result.result.length).toEqual(1);
      });
    });

    describe('when passed `IndicatorName.Apo`', () => {
      test('should return object with #result key', () => {
        const result = service.generateSignalForIndicator(
          ohlcvCloseValueMock,
          IndicatorName.Apo,
          300,
          strategyMock.strategySegments as StrategySegment[],
        );

        expect(result.result.length).toEqual(1);
      });
    });

    describe('when passed `IndicatorName.Ema`', () => {
      test('should return object with #fastPeriodResult and #slowPeriodResult keys', () => {
        const result = service.generateSignalForIndicator(
          ohlcvCloseValueMock,
          IndicatorName.Ema,
          300,
          strategyMock.strategySegments as StrategySegment[],
        );

        expect(result.fastPeriodResult.length).toEqual(1);
        expect(result.slowPeriodResult.length).toEqual(1);
        expect(typeof result.result).toEqual('undefined');
      });
    });

    describe('when passed `IndicatorName.Sma`', () => {
      test('should return object with #fastPeriodResult and #slowPeriodResult keys', () => {
        const result = service.generateSignalForIndicator(
          ohlcvCloseValueMock,
          IndicatorName.Sma,
          900,
          strategyMock.strategySegments as StrategySegment[],
        );

        expect(result.fastPeriodResult.length).toEqual(1);
        expect(result.slowPeriodResult.length).toEqual(1);
        expect(typeof result.result).toEqual('undefined');
      });
    });
  });
});

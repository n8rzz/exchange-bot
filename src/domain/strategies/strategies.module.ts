import { forwardRef, Module } from '@nestjs/common';
import { StrategiesService } from './strategies.service';
import { StrategiesController } from './strategies.controller';
import { strategiesProviders } from './strategies.provider';
import { StrategyConsumer } from './strategy.consumer';
import { QueueModule } from '../../queue/queue.module';

@Module({
  imports: [forwardRef(() => QueueModule)],
  providers: [StrategyConsumer, StrategiesService, ...strategiesProviders],
  controllers: [StrategiesController],
  exports: [StrategiesService],
})
export class StrategiesModule {}

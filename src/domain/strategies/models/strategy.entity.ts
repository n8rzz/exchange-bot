import {
  Table,
  Column,
  Model,
  DataType,
  ForeignKey,
  BelongsTo,
  HasMany,
} from 'sequelize-typescript';
import { Bot } from '../../bots/models/bot.entity';
import { StrategySegment } from '../../strategy-segments/models/strategy-segment.entity';
import { User } from '../../users/models/user.entity';
import { StrategyDto } from './strategy.dto';

@Table
export class Strategy extends Model<StrategyDto> {
  @Column({
    type: DataType.UUID,
    allowNull: false,
    autoIncrement: false,
    defaultValue: DataType.UUIDV4,
    primaryKey: true,
  })
  id: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  name: string;

  @Column({
    type: DataType.TEXT,
    allowNull: true,
  })
  description: string;

  @HasMany(() => StrategySegment)
  strategySegments: StrategySegment[];

  @HasMany(() => Bot)
  bot: Bot[];

  @ForeignKey(() => User)
  @Column({
    type: DataType.UUID,
    allowNull: false,
  })
  userId: string;

  @BelongsTo(() => User)
  user: User;
}

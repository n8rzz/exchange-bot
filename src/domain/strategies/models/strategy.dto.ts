import {
  IsArray,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';
import { StrategySegment } from '../../strategy-segments/models/strategy-segment.entity';

export class CreateStrategyDto {
  @IsString()
  @IsNotEmpty()
  readonly name: string;

  @IsString()
  @IsOptional()
  readonly description: string;

  // TODO: may be unused
  @IsArray()
  @IsOptional()
  readonly periods: number[];

  @IsUUID()
  @IsNotEmpty()
  readonly userId: number;
}

export class StrategyDto extends CreateStrategyDto {
  @IsUUID()
  @IsOptional()
  readonly id?: string;

  @IsUUID()
  @IsOptional()
  readonly botId?: number;

  @IsUUID()
  @IsOptional()
  readonly strategySegments?: StrategySegment[];
}

import { IndicatorName } from '../strategy-segments/strategy-segments.constants';

export interface IPeriodAndIndicator {
  [key: string]: IndicatorName[];
}

import { Injectable, Inject } from '@nestjs/common';
import { Repository } from '../../constants/repository.constants';
import { StrategySegment } from '../strategy-segments/models/strategy-segment.entity';
import { User } from '../users/models/user.entity';
import { CreateStrategyDto, StrategyDto } from './models/strategy.dto';
import { Strategy } from './models/strategy.entity';

@Injectable()
export class StrategiesService {
  constructor(
    @Inject(Repository.Strategy)
    private readonly strategyRepository: typeof Strategy,
  ) {}

  async create(strategy: CreateStrategyDto): Promise<Strategy> {
    return this.strategyRepository.create<Strategy>({
      ...strategy,
      userId: strategy.userId,
    });
  }

  async findAll(): Promise<Strategy[]> {
    return this.strategyRepository.findAll<Strategy>({
      include: [{ model: User, attributes: { exclude: ['password'] } }],
    });
  }

  async findOne(id: string): Promise<Strategy> {
    return this.strategyRepository.findOne({
      where: { id },
      include: [
        {
          model: StrategySegment,
          attributes: {
            exclude: ['createdAt', 'modifiedAt', 'strategyId', 'userId'],
          },
        },
      ],
    });
  }

  async delete(id: string, userId: number) {
    return this.strategyRepository.destroy({ where: { id, userId } });
  }

  async update(id: string, data: Partial<StrategyDto>) {
    const [numberOfAffectedRows, [updatedStrategy]] =
      await this.strategyRepository.update(
        { ...data },
        { where: { id, userId: data.userId }, returning: true },
      );

    return { numberOfAffectedRows, updatedStrategy };
  }
}

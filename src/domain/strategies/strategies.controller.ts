import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Param,
  Body,
  NotFoundException,
  UseGuards,
  Request,
  HttpCode,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBearerAuth,
  ApiTags,
  ApiOperation,
  ApiOkResponse,
  ApiForbiddenResponse,
  ApiCreatedResponse,
  ApiBadRequestResponse,
  ApiNotFoundResponse,
  ApiConflictResponse,
  ApiNoContentResponse,
} from '@nestjs/swagger';
import { ApiTag } from '../../config/swagger.constants';
import { CreateStrategyDto, StrategyDto } from './models/strategy.dto';
import { Strategy } from './models/strategy.entity';
import { StrategiesService } from './strategies.service';

@ApiBearerAuth()
@ApiTags(ApiTag.Strategies)
/**
 * path set via `app.routes`:
 *
 * `api/strategy`
 */
@Controller()
export class StrategiesController {
  constructor(private readonly strategiesService: StrategiesService) {}

  @Get()
  @ApiOperation({ summary: 'list strategies' })
  @ApiOkResponse({
    type: StrategyDto,
    isArray: true,
  })
  @ApiForbiddenResponse()
  async findAll() {
    return this.strategiesService.findAll();
  }

  @Get(':id')
  @ApiOperation({ summary: 'find strategy' })
  @ApiOkResponse({
    type: StrategyDto,
  })
  @ApiForbiddenResponse()
  async findOne(@Param('id') id: string): Promise<Strategy> {
    const post = await this.strategiesService.findOne(id);

    if (!post) {
      throw new NotFoundException("This Post doesn't exist");
    }

    return post;
  }

  @UseGuards(AuthGuard('jwt'))
  @Post()
  @ApiOperation({ summary: 'create Strategy' })
  @ApiCreatedResponse({ type: StrategyDto })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse({ description: 'Not found' })
  @ApiConflictResponse()
  async create(@Body() strategy: CreateStrategyDto): Promise<Strategy> {
    return this.strategiesService.create(strategy);
  }

  @UseGuards(AuthGuard('jwt'))
  @Put(':id')
  @ApiOperation({ summary: 'update strategy' })
  @ApiOkResponse({
    type: StrategyDto,
  })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse({ description: 'Not found' })
  @ApiConflictResponse()
  async update(
    @Param('id') id: string,
    @Body() strategy: StrategyDto,
  ): Promise<Strategy> {
    const { numberOfAffectedRows, updatedStrategy } =
      await this.strategiesService.update(id, strategy);

    if (numberOfAffectedRows === 0) {
      throw new NotFoundException("Strategy doesn't exist");
    }

    return updatedStrategy;
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  @HttpCode(204)
  @ApiOperation({ summary: 'delete strategy' })
  @ApiNoContentResponse()
  @ApiBadRequestResponse()
  @ApiNotFoundResponse({ description: 'Not found' })
  async remove(@Param('id') id: string, @Request() req) {
    const deleted = await this.strategiesService.delete(id, req.user.id);

    if (deleted === 0) {
      throw new NotFoundException("Strategy doesn't exist");
    }

    return 'Successfully deleted';
  }
}

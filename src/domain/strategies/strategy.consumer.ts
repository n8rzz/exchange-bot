import { Processor, Process } from '@nestjs/bull';
import { Job } from 'bull';
import { QueueName } from '../../queue/queue.constants';
import { StrategiesService } from './strategies.service';
import { QueueService } from '../../queue/queue.service';
import { ICreateScreategySchedulesData } from '../../queue/queue.types';
import { extractPeriodAndIndicatorsFromStrategy } from './strategies.utils';
import { Logger } from '@nestjs/common';

@Processor(QueueName.Strategy)
export class StrategyConsumer {
  private readonly _logger = new Logger(StrategyConsumer.name);

  constructor(
    private readonly _queueService: QueueService,
    private readonly _strategyService: StrategiesService,
  ) {}

  @Process()
  async createStrategySchedules(job: Job<ICreateScreategySchedulesData>) {
    this._logger.log(
      `botId: ${job.data.botId}  strategy: ${job.data.strategyId}`,
      'StrategyConsumer.createStrategySchedules',
    );

    const strategyId = job.data.strategyId;
    const strategy = await this._strategyService.findOne(strategyId);
    // TODO: this function should live here or in some other service
    const periodIndicators = extractPeriodAndIndicatorsFromStrategy(strategy);

    job.data.tickers.forEach(async (ticker: string) => {
      Object.keys(periodIndicators).forEach(async (period: string) => {
        await this._queueService.scheduleCandlestickRefresh(
          job.data.botId,
          job.data.exchangeId,
          strategy,
          ticker,
          +period,
          periodIndicators[period],
        );
      });
    });
  }
}

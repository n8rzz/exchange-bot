import { StrategySegment } from '../strategy-segments/models/strategy-segment.entity';
import { Strategy } from './models/strategy.entity';
import { IPeriodAndIndicator } from './strategy.types';

export const extractPeriodAndIndicatorsFromStrategy = (
  strategy: Strategy,
): IPeriodAndIndicator => {
  return strategy.strategySegments.reduce(
    (sum: IPeriodAndIndicator, segment: StrategySegment) => {
      if (sum[segment.period] == null) {
        sum[segment.period] = [segment.indicator];

        return sum;
      }

      if (sum[segment.period].includes(segment.indicator)) {
        return sum;
      }

      sum[segment.period].push(segment.indicator);

      return sum;
    },
    {},
  );
};

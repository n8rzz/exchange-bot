import { Repository } from '../../constants/repository.constants';
import { Strategy } from './models/strategy.entity';

export const strategiesProviders = [
  {
    provide: Repository.Strategy,
    useValue: Strategy,
  },
];

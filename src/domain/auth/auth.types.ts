import { UserDto } from '../users/models/user.dto';

export interface IAuthResponse {
  user: UserDto;
  token: string;
}

import {
  Controller,
  Body,
  Post,
  UseGuards,
  Request,
  HttpCode,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiTags,
  ApiOperation,
  ApiOkResponse,
  ApiConflictResponse,
} from '@nestjs/swagger';
import { DoesUserExist } from '../../guards/doesUserExist.guard';
import { CreateUserDto } from '../users/models/user.dto';
import { IAuthResponse } from './auth.types';
import { AuthService } from './auth.service';
import { ApiTag } from '../../config/swagger.constants';

@ApiTags(ApiTag.Authentication)
@Controller('api/auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(AuthGuard('local'))
  @Post('login')
  @ApiOperation({ summary: 'start session' })
  @ApiOkResponse({
    description: 'The found record',
  })
  @ApiConflictResponse({
    status: 403,
    description: 'The found record',
  })
  @HttpCode(200)
  async login(@Request() req): Promise<IAuthResponse> {
    return this.authService.login(req.user);
  }

  @UseGuards(DoesUserExist)
  @Post('signup')
  @ApiOperation({ summary: 'create user and start session' })
  @ApiOkResponse({
    description: 'The found record',
  })
  async signUp(@Body() user: CreateUserDto): Promise<IAuthResponse> {
    return this.authService.create(user);
  }
}

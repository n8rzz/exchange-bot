import * as bcrypt from 'bcrypt';
import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { UserDto } from '../users/models/user.dto';
import { IAuthResponse } from './auth.types';

@Injectable()
export class AuthService {
  constructor(
    private readonly _userService: UsersService,
    private readonly _jwtService: JwtService,
  ) {}

  async validateUser(username: string, pass: string) {
    const user = await this._userService.findOneByEmail(username);

    if (!user) {
      return null;
    }

    const match = await this._comparePassword(pass, user.password);

    if (!match) {
      return null;
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { password, ...result } = user['dataValues'];

    return result;
  }

  public async login(user: UserDto): Promise<IAuthResponse> {
    const token = await this._generateToken(user);

    return { user, token };
  }

  public async create(user: UserDto): Promise<IAuthResponse> {
    const pass = await this._hashPassword(user.password);
    const newUser = await this._userService.create({ ...user, password: pass });
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { password, ...result } = newUser['dataValues'];
    const token = await this._generateToken(result);

    return { user: result, token };
  }

  private async _generateToken(user: UserDto): Promise<string> {
    return this._jwtService.signAsync(user);
  }

  private async _hashPassword(password: string): Promise<string> {
    return bcrypt.hash(password, 10);
  }

  private async _comparePassword(
    passToCompare: string,
    storedPassword: string,
  ) {
    return bcrypt.compare(passToCompare, storedPassword);
  }
}

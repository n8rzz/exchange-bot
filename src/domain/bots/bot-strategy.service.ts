import { Injectable, Logger } from '@nestjs/common';
import { QueueService } from '../../queue/queue.service';
import { IRefreshSignalsForBotJobData } from '../../queue/queue.types';
import { Signal } from '../signals/models/signal.entity';
import { SignalsService } from '../signals/signals.service';
import { StrategySegment } from '../strategy-segments/models/strategy-segment.entity';
import { StrategySegmentType } from '../strategy-segments/strategy-segments.constants';
import { BotsService } from './bots.service';
import { Bot } from './models/bot.entity';

@Injectable()
export class BotStrategyService {
  private readonly _logger = new Logger(BotStrategyService.name);

  constructor(
    private readonly _botService: BotsService,
    private readonly _queueService: QueueService,
    private readonly _signalService: SignalsService,
  ) {}

  async refreshSignalsForBot(jobData: IRefreshSignalsForBotJobData) {
    const bot = await this._botService.findOne(jobData.botId);
    const botBuySegments = bot.strategy.strategySegments.filter(
      (segment: StrategySegment) => segment.type === StrategySegmentType.Buy,
    );
    const botSellSegments = bot.strategy.strategySegments.filter(
      (segment: StrategySegment) => segment.type === StrategySegmentType.Sell,
    );

    bot.tickers.forEach(async (ticker: string) => {
      await this._generateBuyOrSellOrdersForTicker(
        ticker,
        bot,
        botBuySegments,
        botSellSegments,
      );
    });
  }

  private async _generateBuyOrSellOrdersForTicker(
    ticker: string,
    bot: Bot,
    buySegments: StrategySegment[],
    sellSegments: StrategySegment[],
  ): Promise<void> {
    const signalsForTicker =
      await this._signalService.findRecentSignalsForTicker(bot.id, ticker);

    const shouldBuy = this._calculateBotSignalForSegmentType(
      buySegments,
      signalsForTicker,
      StrategySegmentType.Buy,
      ticker,
    );
    const shouldSell = this._calculateBotSignalForSegmentType(
      sellSegments,
      signalsForTicker,
      StrategySegmentType.Sell,
      ticker,
    );

    if (shouldBuy) {
      await this._queueService.generateOrderForBotSignal(
        bot.id,
        ticker,
        StrategySegmentType.Buy,
      );
    }

    if (shouldSell) {
      await this._queueService.generateOrderForBotSignal(
        bot.id,
        ticker,
        StrategySegmentType.Sell,
      );
    }
  }

  private _calculateBotSignalForSegmentType(
    botSegments: StrategySegment[],
    signals: Signal[],
    segmentType: StrategySegmentType,
    ticker: string,
  ): boolean {
    const segmentIdsWithSignal = this._findSignalsForSegmentType(
      botSegments,
      signals,
      segmentType,
    );
    const requiredSegmentIds = botSegments
      .filter((segment: StrategySegment) => segment.isRequired)
      .map((v) => v.id);

    console.log(`--- ticker: ${ticker}  segmentType: ${segmentType}`);
    console.log('--- requiredSegmentIds', requiredSegmentIds);
    console.log('--- segmentIdsWithSignal', segmentIdsWithSignal);

    if (requiredSegmentIds.length === 0) {
      return false;
    }

    return requiredSegmentIds
      .map((id: string) => segmentIdsWithSignal.includes(id))
      .every(Boolean);
  }

  /**
   * Loops through a list of `StrategySegments` with the same `type`, in
   * attempting to find a `Signal` for each segment
   *
   * @param segments {StrategySegment[]}
   * @param signals {Signal[]}
   * @param segmentType {StrategySegmentType}
   * @returns {string[]}  `StrategySegmentId` or `''`
   */
  private _findSignalsForSegmentType(
    segments: StrategySegment[],
    signals: Signal[],
    segmentType: StrategySegmentType,
  ): string[] {
    return segments.map((segment: StrategySegment): string => {
      const signal = signals.find(
        (signal: Signal) => signal.strategySegmentId === segment.id,
      );

      return signal?.signalResult === segmentType ? segment.id : '';
    });
  }
}

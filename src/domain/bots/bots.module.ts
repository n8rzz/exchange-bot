import { forwardRef, Module } from '@nestjs/common';
import { BotsService } from './bots.service';
import { BotsController } from './bots.controller';
import { botsProviders } from './bots.provider';
import { BotCreateInterceptor } from './interceptors/bot-create.interceptor';
import { BotUpdateInterceptor } from './interceptors/bot-update.interceptor';
import { BotDestroyInterceptor } from './interceptors/bot-destroy.interceptor';
import { BotConsumer } from './bot.consumer';
import { QueueModule } from '../../queue/queue.module';
import { SignalsModule } from '../signals/signals.module';
import { BotStrategyService } from './bot-strategy.service';

@Module({
  imports: [forwardRef(() => QueueModule), SignalsModule],
  providers: [
    BotConsumer,
    BotCreateInterceptor,
    BotUpdateInterceptor,
    BotDestroyInterceptor,
    BotsService,
    BotStrategyService,
    ...botsProviders,
  ],
  controllers: [BotsController],
  exports: [BotsService],
})
export class BotsModule {}

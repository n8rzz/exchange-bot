import { Injectable, Inject } from '@nestjs/common';
import { Repository } from '../../constants/repository.constants';
import { Strategy } from '../strategies/models/strategy.entity';
import { StrategySegment } from '../strategy-segments/models/strategy-segment.entity';
import { BotDto, CreateBotDto } from './models/bot.dto';
import { Bot } from './models/bot.entity';

export interface IBotsService {
  create: (bot: CreateBotDto) => Promise<Bot>;
  startOrStop: (
    botId: string,
    isActive: boolean,
  ) => Promise<{ numberOfAffectedRows: number; updatedBot: Bot }>;
  findAll: () => Promise<Bot[]>;
  findOne: (id: string) => Promise<Bot>;
  delete: (id: string, userId: string) => Promise<number>;
  update: (
    id: string,
    data: Partial<BotDto>,
  ) => Promise<{ numberOfAffectedRows: number; updatedBot: Bot }>;
}

@Injectable()
export class BotsService implements IBotsService {
  constructor(
    @Inject(Repository.Bot)
    private readonly botRepository: typeof Bot,
  ) {}

  async create(bot: CreateBotDto): Promise<Bot> {
    return this.botRepository.create<Bot>({
      ...bot,
      userId: bot.userId,
    });
  }

  async startOrStop(
    botId: string,
    isActive: boolean,
  ): Promise<{ numberOfAffectedRows: number; updatedBot: Bot }> {
    const [numberOfAffectedRows, [updatedBot]] =
      await this.botRepository.update<Bot>(
        { isActive },
        { where: { id: botId }, returning: true },
      );

    return { numberOfAffectedRows, updatedBot };
  }

  async findAll(): Promise<Bot[]> {
    return this.botRepository.findAll<Bot>({
      attributes: { exclude: ['strategyId'] },
      include: [
        {
          model: Strategy,
          attributes: { exclude: ['createdAt', 'modifiedAt', 'userId'] },
        },
      ],
    });
  }

  async findOne(id: string): Promise<Bot> {
    return this.botRepository.findOne({
      where: { id },
      attributes: { exclude: ['strategyId'] },
      include: [
        {
          model: Strategy,
          include: [
            {
              model: StrategySegment,
              attributes: {
                exclude: ['createdAt', 'modifiedAt', 'userId', 'strategyId'],
              },
            },
          ],
          attributes: { exclude: ['createdAt', 'modifiedAt', 'userId'] },
        },
      ],
    });
  }

  async delete(id: string, userId: string): Promise<number> {
    return this.botRepository.destroy({ where: { id, userId } });
  }

  async update(id: string, data: Partial<BotDto>) {
    const [numberOfAffectedRows, [updatedBot]] =
      await this.botRepository.update(
        { ...data },
        { where: { id, userId: data.userId }, returning: true },
      );

    return { numberOfAffectedRows, updatedBot };
  }
}

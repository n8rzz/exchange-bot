import {
  ComparisonSymbol,
  IndicatorName,
  StrategySegmentType,
} from '../../strategy-segments/strategy-segments.constants';
import { IBot } from '../bots.types';

const rsiBuyStrategySegment = {
  id: '7219ff24-42f0-44eb-bebd-91d87097c716',
  indicator: IndicatorName.Rsi,
  isRequired: true,
  keepSignalLength: 1,
  params: {
    period: 9,
    signalWhen: {
      comparison: ComparisonSymbol.GreaterThanOrEqualTo,
      value: 70,
    },
  },
  period: 900,
  type: StrategySegmentType.Buy,
  updatedAt: '2021-06-11T04:46:28.146Z',
};

const rsiSellStrategySegment = {
  id: '7219ff24-42f0-44eb-bebd-91d87097c716',
  indicator: IndicatorName.Rsi,
  isRequired: true,
  keepSignalLength: 1,
  params: {
    period: 9,
    signalWhen: {
      comparison: ComparisonSymbol.GreaterThanOrEqualTo,
      value: 70,
    },
  },
  period: 900,
  type: StrategySegmentType.Sell,
  updatedAt: '2021-06-11T04:46:28.146Z',
};

export const botWithBuySegmentMock: IBot = {
  id: 'c469a093-95bc-489a-a375-e8862d0308ca',
  exchangeId: 'aedb39a8-c898-4377-b149-d6e73da4dbb4',
  isActive: true,
  isPaper: true,
  maxAllocationAmount: '100.00',
  maxOpenBuyOrderTime: 10,
  maxOpenPositions: 10,
  maxOpenSellOrderTime: 10,
  minOrderAmount: '100.00',
  name: 'Test Bot',
  numberOfTargets: 3,
  shouldNotifyOnCancel: true,
  shouldNotifyOnError: true,
  shouldNotifyOnTrade: true,
  tickers: ['ETH/USDT'],
  userId: 'e32697a8-9f71-4c2e-9993-6ac5634da2c8',
  strategy: {
    id: '0722781c-9011-4232-9c48-5278b95a69ea',
    name: 'rsi-over-under',
    description: null,
    strategySegments: [rsiBuyStrategySegment as any],
  } as any,
};

export const botWithSellSegmentMock: IBot = {
  ...botWithBuySegmentMock,
  strategy: {
    strategySegments: [rsiSellStrategySegment as any],
  } as any,
};

export const signalListMock = [
  {
    id: '48913a76-d17f-49f5-816b-6a79c3253be7',
    analysisTime: '2021-06-11T22:58:35.468Z',
    indicatorName: rsiBuyStrategySegment.indicator,
    isRequired: true,
    params: {
      result: [100],
      strategySegmentId: rsiBuyStrategySegment.id,
      isRequired: true,
      signal: StrategySegmentType.Buy,
    },
    signalResult: StrategySegmentType.Buy,
    ticker: 'ETH/USDT',
    strategySegmentId: rsiBuyStrategySegment.id,
    botId: '212682bc-b726-4eb6-b674-50f53cd2d654',
    exchangeId: 'b171747a-801e-4a7a-a0fc-9907023f4a87',
    strategySegment: { ...rsiBuyStrategySegment },
  },
];

export const signalListWithSellMock = [
  {
    id: '48913a76-d17f-49f5-816b-6a79c3253be7',
    analysisTime: '2021-06-11T22:58:35.468Z',
    indicatorName: rsiBuyStrategySegment.indicator,
    isRequired: true,
    params: {
      result: [100],
      strategySegmentId: rsiBuyStrategySegment.id,
      isRequired: true,
      signal: StrategySegmentType.Sell,
    },
    signalResult: StrategySegmentType.Sell,
    ticker: 'ETH/USDT',
    strategySegmentId: rsiBuyStrategySegment.id,
    botId: '212682bc-b726-4eb6-b674-50f53cd2d654',
    exchangeId: 'b171747a-801e-4a7a-a0fc-9907023f4a87',
    strategySegment: { ...rsiBuyStrategySegment },
  },
];

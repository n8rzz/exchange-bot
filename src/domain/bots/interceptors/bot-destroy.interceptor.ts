import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { QueueService } from '../../../queue/queue.service';

@Injectable()
export class BotDestroyInterceptor implements NestInterceptor {
  constructor(private readonly _queueService: QueueService) {}

  intercept(context: ExecutionContext, next$: CallHandler): Observable<any> {
    const ctx = context.switchToHttp();
    const request = ctx.getRequest();
    const { statusCode } = ctx.getResponse();

    return next$.handle().pipe(
      map((content) => {
        if (statusCode !== 204) {
          return content;
        }

        this._queueService.destroyStrategySchedules(request.params.id);

        return content;
      }),
    );
  }
}

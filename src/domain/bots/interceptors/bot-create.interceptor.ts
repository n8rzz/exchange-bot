import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { QueueService } from '../../../queue/queue.service';

@Injectable()
export class BotCreateInterceptor implements NestInterceptor {
  constructor(private readonly _queueService: QueueService) {}

  intercept(context: ExecutionContext, next$: CallHandler): Observable<any> {
    const ctx = context.switchToHttp();
    const { statusCode } = ctx.getResponse();

    return next$.handle().pipe(
      map(async (content) => {
        if (statusCode !== 201) {
          return content;
        }

        await this._queueService.createStrategySchedules(
          content.dataValues.id,
          content.dataValues.exchangeId,
          content.dataValues.strategyId,
          content.dataValues.tickers,
        );

        return content;
      }),
    );
  }
}

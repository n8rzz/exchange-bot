import { Patch } from '@nestjs/common';
import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Param,
  Body,
  NotFoundException,
  UseGuards,
  Request,
  HttpCode,
  UseInterceptors,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBearerAuth,
  ApiTags,
  ApiOperation,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiNoContentResponse,
  ApiCreatedResponse,
  ApiBadRequestResponse,
  ApiConflictResponse,
  ApiForbiddenResponse,
} from '@nestjs/swagger';
import { ApiTag } from '../../config/swagger.constants';
import { BotsService } from './bots.service';
import { BotCreateInterceptor } from './interceptors/bot-create.interceptor';
import { BotDestroyInterceptor } from './interceptors/bot-destroy.interceptor';
import { BotUpdateInterceptor } from './interceptors/bot-update.interceptor';
import { BotDto, CreateBotDto } from './models/bot.dto';
import { Bot } from './models/bot.entity';

@ApiBearerAuth()
@ApiTags(ApiTag.Bots)
/**
 * path set via `app.routes`:
 *
 * `api/bots`
 */
@Controller()
export class BotsController {
  constructor(private readonly _botsService: BotsService) {}

  @UseGuards(AuthGuard('jwt'))
  @Get()
  @ApiOperation({ summary: 'list bots' })
  @ApiOkResponse({
    type: BotDto,
    isArray: true,
  })
  @ApiForbiddenResponse()
  async findAll() {
    return this._botsService.findAll();
  }

  @UseGuards(AuthGuard('jwt'))
  @Get(':id')
  @ApiOperation({ summary: 'find bot' })
  @ApiOkResponse({
    type: BotDto,
  })
  @ApiForbiddenResponse()
  async findOne(@Param('id') id: string): Promise<Bot> {
    const post = await this._botsService.findOne(id);

    if (!post) {
      throw new NotFoundException("This Post doesn't exist");
    }

    return post;
  }

  @UseGuards(AuthGuard('jwt'))
  @Post()
  @ApiOperation({ summary: 'create bot' })
  @ApiCreatedResponse({ type: BotDto })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse({ description: 'Not found' })
  @ApiConflictResponse()
  @UseInterceptors(BotCreateInterceptor)
  async create(@Body() bot: CreateBotDto): Promise<Bot> {
    return this._botsService.create(bot);
  }

  @UseGuards(AuthGuard('jwt'))
  @Patch(':id/start')
  @ApiOperation({ summary: 'start bot' })
  @ApiOkResponse({ type: BotDto })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse({ description: 'Not found' })
  @ApiConflictResponse()
  @UseInterceptors(BotUpdateInterceptor)
  async start(@Param('id') botId: string): Promise<Bot> {
    const { numberOfAffectedRows, updatedBot } =
      await this._botsService.startOrStop(botId, true);

    if (numberOfAffectedRows === 0) {
      throw new NotFoundException("Bot doesn't exist");
    }

    return updatedBot;
  }

  @UseGuards(AuthGuard('jwt'))
  @Patch(':id/stop')
  @ApiOperation({ summary: 'stop bot' })
  @ApiOkResponse({ type: BotDto })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse({ description: 'Not found' })
  @ApiConflictResponse()
  @UseInterceptors(BotUpdateInterceptor)
  async stop(@Param('id') botId: string): Promise<Bot> {
    const { numberOfAffectedRows, updatedBot } =
      await this._botsService.startOrStop(botId, false);

    if (numberOfAffectedRows === 0) {
      throw new NotFoundException("Bot doesn't exist");
    }

    return updatedBot;
  }

  @UseGuards(AuthGuard('jwt'))
  @Put(':id')
  @ApiOperation({ summary: 'update bot' })
  @ApiOkResponse({
    type: BotDto,
  })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse({ description: 'Not found' })
  @ApiConflictResponse()
  @UseInterceptors(BotUpdateInterceptor)
  async update(@Param('id') id: string, @Body() bot: BotDto): Promise<Bot> {
    const { numberOfAffectedRows, updatedBot } = await this._botsService.update(
      id,
      bot,
    );

    if (numberOfAffectedRows === 0) {
      throw new NotFoundException("Bot doesn't exist");
    }

    return updatedBot;
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  @ApiOperation({ summary: 'delete bot' })
  @ApiNoContentResponse()
  @ApiBadRequestResponse()
  @ApiNotFoundResponse({ description: 'Not found' })
  @HttpCode(204)
  @UseInterceptors(BotDestroyInterceptor)
  async remove(@Param('id') id: string, @Request() req) {
    const deleted = await this._botsService.delete(id, req.user.id);

    if (deleted === 0) {
      throw new NotFoundException("Bot doesn't exist");
    }

    return 'Successfully deleted';
  }
}

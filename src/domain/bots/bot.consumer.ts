import { Processor, Process } from '@nestjs/bull';
import { Logger } from '@nestjs/common';
import { Job } from 'bull';
import { QueueName } from '../../queue/queue.constants';
import { IRefreshSignalsForBotJobData } from '../../queue/queue.types';
import { BotStrategyService } from './bot-strategy.service';

@Processor(QueueName.Bot)
export class BotConsumer {
  private readonly _logger = new Logger(BotConsumer.name);

  constructor(private readonly _botStrategyService: BotStrategyService) {}

  @Process()
  async refreshSignalsForBot(job: Job<IRefreshSignalsForBotJobData>) {
    this._logger.log(
      `botId: ${job.data.botId}`,
      'BotConsumer.refreshSignalsForBot',
    );

    this._botStrategyService.refreshSignalsForBot(job.data);
  }
}

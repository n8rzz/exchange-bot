import { Test, TestingModule } from '@nestjs/testing';
import { QueueService } from '../../queue/queue.service';
import { SignalsService } from '../signals/signals.service';
import { StrategySegmentType } from '../strategy-segments/strategy-segments.constants';
import { BotStrategyService } from './bot-strategy.service';
import { BotsService } from './bots.service';
import {
  botWithBuySegmentMock,
  botWithSellSegmentMock,
  signalListMock,
  signalListWithSellMock,
} from './__mocks__/bot.mocks';

describe('BotStrategyService', () => {
  let service: BotStrategyService;
  let botServiceFindOneSpy;
  let generateOrderForBotSignalSpy;
  let findRecentSignalsForTickerSpy;

  const botServiceFake = { findOne: jest.fn() };
  const queueServiceFake = {
    generateOrderForBotSignal: jest.fn(),
  };
  const signalServiceFake = {
    findRecentSignalsForTicker: jest.fn().mockResolvedValue([]),
  };

  beforeEach(async () => {
    botServiceFindOneSpy = jest.fn().mockResolvedValue(botWithBuySegmentMock);
    generateOrderForBotSignalSpy = jest.fn();
    findRecentSignalsForTickerSpy = jest.fn().mockResolvedValue([]);

    botServiceFake.findOne = botServiceFindOneSpy;
    queueServiceFake.generateOrderForBotSignal = generateOrderForBotSignalSpy;
    signalServiceFake.findRecentSignalsForTicker =
      findRecentSignalsForTickerSpy;

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BotStrategyService,
        {
          provide: BotsService,
          useFactory: () => botServiceFake,
        },
        {
          provide: QueueService,
          useFactory: () => queueServiceFake,
        },
        {
          provide: SignalsService,
          useFactory: () => signalServiceFake,
        },
      ],
    }).compile();

    service = module.get<BotStrategyService>(BotStrategyService);
  });

  afterEach(() => {
    botServiceFindOneSpy = null;
    generateOrderForBotSignalSpy = null;
    findRecentSignalsForTickerSpy = null;
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('when bot has no #tickers or #strategySegments', () => {
    it('calls BotService with botId', async (done) => {
      botServiceFake.findOne = jest.fn(() =>
        Promise.resolve({
          ...botWithBuySegmentMock,
          tickers: [],
          strategy: {
            strategySegments: [],
          },
        }),
      );
      await service.refreshSignalsForBot({ botId: botWithBuySegmentMock.id });

      expect(botServiceFake.findOne).toHaveBeenCalledWith(
        botWithBuySegmentMock.id,
      );
      done();
    });
  });

  describe('when bot has #ticker and #strategySegments', () => {
    it('calls BotService with botId', async (done) => {
      await service.refreshSignalsForBot({ botId: botWithBuySegmentMock.id });

      expect(botServiceFake.findOne).toHaveBeenCalledWith(
        botWithBuySegmentMock.id,
      );
      done();
    });

    it('calls SignalService with correct params', async (done) => {
      signalServiceFake.findRecentSignalsForTicker = jest
        .fn()
        .mockResolvedValueOnce(signalListMock);

      await service.refreshSignalsForBot({ botId: botWithBuySegmentMock.id });

      expect(signalServiceFake.findRecentSignalsForTicker).toHaveBeenCalledWith(
        botWithBuySegmentMock.id,
        signalListMock[0].ticker,
      );
      done();
    });

    it('calls SignalService correct number of times', async (done) => {
      signalServiceFake.findRecentSignalsForTicker = jest
        .fn()
        .mockResolvedValueOnce(signalListMock);

      await service.refreshSignalsForBot({ botId: botWithBuySegmentMock.id });

      expect(
        signalServiceFake.findRecentSignalsForTicker,
      ).toHaveBeenCalledTimes(1);
      done();
    });

    it('calls QueueService with #botId and #ticker with Buy type', async (done) => {
      signalServiceFake.findRecentSignalsForTicker = jest
        .fn()
        .mockReturnValue(signalListMock);

      await service.refreshSignalsForBot({ botId: botWithBuySegmentMock.id });

      expect(
        queueServiceFake.generateOrderForBotSignal,
      ).toHaveBeenLastCalledWith(
        botWithBuySegmentMock.id,
        botWithBuySegmentMock.tickers[0],
        StrategySegmentType.Buy,
      );
      expect(
        queueServiceFake.generateOrderForBotSignal,
      ).not.toHaveBeenLastCalledWith(
        botWithBuySegmentMock.id,
        botWithBuySegmentMock.tickers,
        StrategySegmentType.Sell,
      );
      done();
    });

    it('calls QueueService with #botId and #ticker with Sell type', async (done) => {
      botServiceFake.findOne = jest
        .fn()
        .mockResolvedValue(botWithSellSegmentMock);
      signalServiceFake.findRecentSignalsForTicker = jest
        .fn()
        .mockReturnValue(signalListWithSellMock);

      await service.refreshSignalsForBot({ botId: botWithBuySegmentMock.id });

      expect(
        queueServiceFake.generateOrderForBotSignal,
      ).toHaveBeenLastCalledWith(
        botWithBuySegmentMock.id,
        botWithBuySegmentMock.tickers[0],
        StrategySegmentType.Sell,
      );
      expect(
        queueServiceFake.generateOrderForBotSignal,
      ).not.toHaveBeenLastCalledWith(
        botWithBuySegmentMock.id,
        botWithBuySegmentMock.tickers[0],
        StrategySegmentType.Buy,
      );
      done();
    });
  });
});

import {
  IsArray,
  IsBoolean,
  IsNotEmpty,
  IsNumber,
  IsString,
  Min,
  Max,
  IsUUID,
} from 'class-validator';
import { Strategy } from '../../strategies/models/strategy.entity';
import { UserDto } from '../../users/models/user.dto';
import { IBot } from '../bots.types';

export class CreateBotDto implements Omit<IBot, 'id'> {
  @IsUUID()
  @IsNotEmpty()
  readonly exchangeId: string;

  @IsBoolean()
  @IsNotEmpty()
  readonly isActive: boolean;

  @IsBoolean()
  @IsNotEmpty()
  readonly isPaper: boolean;

  @IsString()
  @IsNotEmpty()
  readonly maxAllocationAmount: string;

  @IsNumber()
  @IsNotEmpty()
  readonly maxOpenBuyOrderTime: number;

  @IsNumber()
  @IsNotEmpty()
  readonly maxOpenPositions: number;

  @IsNumber()
  @IsNotEmpty()
  readonly maxOpenSellOrderTime: number;

  @IsString()
  @IsNotEmpty()
  readonly minOrderAmount: string;

  @IsString()
  @IsNotEmpty()
  readonly name: string;

  @IsNumber()
  @IsNotEmpty()
  @Min(0)
  @Max(10)
  readonly numberOfTargets: number;

  @IsBoolean()
  @IsNotEmpty()
  readonly shouldNotifyOnCancel: boolean;

  @IsBoolean()
  @IsNotEmpty()
  readonly shouldNotifyOnError: boolean;

  @IsBoolean()
  @IsNotEmpty()
  readonly shouldNotifyOnTrade: boolean;

  @IsUUID()
  @IsNotEmpty()
  readonly strategyId: string;

  @IsArray()
  @IsNotEmpty()
  readonly tickers: string[];

  @IsUUID()
  @IsNotEmpty()
  readonly userId: string;
}

export class BotDto extends CreateBotDto {
  @IsUUID()
  readonly id?: string;

  readonly strategy?: Strategy;

  readonly user?: UserDto;
}

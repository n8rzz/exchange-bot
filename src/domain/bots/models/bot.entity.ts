import {
  Table,
  Column,
  Model,
  DataType,
  ForeignKey,
  BelongsTo,
  HasMany,
} from 'sequelize-typescript';
import { Exchange } from '../../exchanges/models/exchange.entity';
import { Signal } from '../../signals/models/signal.entity';
import { Strategy } from '../../strategies/models/strategy.entity';
import { User } from '../../users/models/user.entity';
import { BotDto } from './bot.dto';

@Table
export class Bot extends Model<BotDto> {
  @Column({
    type: DataType.UUID,
    allowNull: false,
    autoIncrement: false,
    defaultValue: DataType.UUIDV4,
    primaryKey: true,
  })
  id: string;

  @ForeignKey(() => Exchange)
  @Column({
    type: DataType.UUID,
    allowNull: false,
  })
  exchangeId: string;

  @Column({
    type: DataType.BOOLEAN,
    allowNull: false,
    defaultValue: true,
  })
  isActive: boolean;

  @Column({
    type: DataType.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  })
  isPaper: boolean;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  maxAllocationAmount: string;

  @Column({
    type: DataType.INTEGER,
    allowNull: false,
    defaultValue: 5,
  })
  maxOpenBuyOrderTime: number;

  @Column({
    type: DataType.INTEGER,
    allowNull: false,
    defaultValue: 5,
  })
  maxOpenPositions: number;

  @Column({
    type: DataType.INTEGER,
    allowNull: false,
    defaultValue: 5,
  })
  maxOpenSellOrderTime: number;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  minOrderAmount: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  name: string;

  @Column({
    type: DataType.INTEGER,
    allowNull: false,
    defaultValue: 3,
  })
  numberOfTargets: number;

  @Column({
    type: DataType.BOOLEAN,
    allowNull: false,
    defaultValue: true,
  })
  shouldNotifyOnCancel: boolean;

  @Column({
    type: DataType.BOOLEAN,
    allowNull: false,
    defaultValue: true,
  })
  shouldNotifyOnError: boolean;

  @Column({
    type: DataType.BOOLEAN,
    allowNull: false,
    defaultValue: true,
  })
  shouldNotifyOnTrade: boolean;

  @Column({
    type: DataType.ARRAY(DataType.STRING),
    allowNull: true,
  })
  tickers: string[];

  @ForeignKey(() => Strategy)
  @Column({
    type: DataType.UUID,
    allowNull: false,
  })
  strategyId: string;

  @BelongsTo(() => Strategy)
  strategy: Strategy;

  @ForeignKey(() => User)
  @Column({
    type: DataType.UUID,
    allowNull: false,
  })
  userId: string;

  @BelongsTo(() => User)
  user: User;

  @HasMany(() => Signal)
  signals: Signal;
}

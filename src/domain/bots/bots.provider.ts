import { Repository } from '../../constants/repository.constants';
import { Bot } from './models/bot.entity';

export const botsProviders = [
  {
    provide: Repository.Bot,
    useValue: Bot,
  },
];

import { Signal } from '../signals/models/signal.entity';
import { Strategy } from '../strategies/models/strategy.entity';
import { UserDto } from '../users/models/user.dto';

export interface ISignalsByTicker {
  [key: string]: Signal[];
}

export interface IBot {
  exchangeId: string;
  id?: string;
  isActive: boolean;
  isPaper: boolean;
  maxAllocationAmount: string;
  maxOpenBuyOrderTime: number;
  maxOpenPositions: number;
  maxOpenSellOrderTime: number;
  minOrderAmount: string;
  name: string;
  numberOfTargets: number;
  shouldNotifyOnCancel: boolean;
  shouldNotifyOnError: boolean;
  shouldNotifyOnTrade: boolean;
  strategy?: Strategy;
  strategyId?: string;
  tickers: string[];
  user?: UserDto;
  userId: string;
}

import { Test, TestingModule } from '@nestjs/testing';
import { QueueService } from '../../queue/queue.service';
import { BotsController } from './bots.controller';
import { BotsService } from './bots.service';
import { BotCreateInterceptor } from './interceptors/bot-create.interceptor';
import { BotDestroyInterceptor } from './interceptors/bot-destroy.interceptor';
import { BotUpdateInterceptor } from './interceptors/bot-update.interceptor';

describe('BotsController', () => {
  let controller: BotsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BotsController],
      providers: [
        {
          provide: QueueService,
          useValue: jest.fn(),
        },
        {
          provide: BotsService,
          useValue: jest.fn(),
        },
        {
          provide: BotCreateInterceptor,
          useValue: jest.fn(),
        },
        {
          provide: BotUpdateInterceptor,
          useValue: jest.fn(),
        },
        {
          provide: BotDestroyInterceptor,
          useValue: jest.fn(),
        },
      ],
    }).compile();

    controller = module.get<BotsController>(BotsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

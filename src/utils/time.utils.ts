const ONE_SECOND_IN_MILLISECONDS = 1000;

export const translatePeriodToDelayInterval = (
  periodInSeconds: number,
): number => {
  return periodInSeconds * ONE_SECOND_IN_MILLISECONDS;
};

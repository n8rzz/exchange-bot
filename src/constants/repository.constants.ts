export enum Repository {
  Auth = 'AUTH_REPOSITORY',
  Bot = 'BOT_REPOSITORY',
  Exchange = 'EXCHANGE_REPOSITORY',
  Order = 'ORDER',
  Signal = 'SIGNAL_REPOSITORY',
  Strategy = 'STRATEGY_REPOSITORY',
  StrategySegment = 'STRATEGY_SEGMENTS_REPOSITORY',
  User = 'USER_REPOSITORY',
  UserExchangeAccount = 'USER_EXCHANGE_ACCOUNT_REPOSITORY',
}

import { Sequelize } from 'sequelize-typescript';
import { Bot } from '../domain/bots/models/bot.entity';
import { Exchange } from '../domain/exchanges/models/exchange.entity';
import { Order } from '../domain/orders/models/order.entity';
import { Signal } from '../domain/signals/models/signal.entity';
import { Strategy } from '../domain/strategies/models/strategy.entity';
import { StrategySegment } from '../domain/strategy-segments/models/strategy-segment.entity';
import { UserExchangeAccount } from '../domain/user-exchange-accounts/models/user-exchange-account.entity';
import { User } from '../domain/users/models/user.entity';
import { databaseConfig } from './database.constants';

export const databaseProviders = [
  {
    provide: 'SEQUELIZE',
    useFactory: async () => {
      const config =
        databaseConfig[process.env.NODE_ENV] ?? databaseConfig.development;

      const sequelize = new Sequelize(config);

      sequelize.addModels([
        Bot,
        Exchange,
        Signal,
        Strategy,
        StrategySegment,
        User,
        UserExchangeAccount,
        Order,
      ]);

      return sequelize;
    },
  },
];

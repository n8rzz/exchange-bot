'use strict';

/**
 * For a list of exchanges
 *
 * see: https://github.com/ccxt/ccxt/wiki/Manual#exchanges
 */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Exchanges', [
      // typically added as part of the postman test suite
      // {
      //   name: 'binanceus',
      // },
      {
        name: 'bitfinex',
      },
      {
        name: 'bitfinex2',
      },
      {
        name: 'coinbase',
      },
      {
        name: 'kraken',
      },
      {
        name: 'poloniex',
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Exchanges', null, {});
  },
};

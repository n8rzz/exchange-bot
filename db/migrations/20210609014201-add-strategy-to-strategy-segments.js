'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('StrategySegments', 'strategyId', {
      type: Sequelize.UUID,
      references: {
        model: 'Strategies',
        key: 'id',
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('StrategySegments', 'strategyId');
  },
};

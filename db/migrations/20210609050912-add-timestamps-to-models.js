'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('Users', 'createdAt', {
        allowNull: false,
        defaultValue: Sequelize.fn('now'),
        type: Sequelize.DATE,
      }),
      queryInterface.addColumn('Users', 'updatedAt', {
        allowNull: false,
        defaultValue: Sequelize.fn('now'),
        type: Sequelize.DATE,
      }),
      queryInterface.addColumn('Bots', 'createdAt', {
        allowNull: false,
        defaultValue: Sequelize.fn('now'),
        type: Sequelize.DATE,
      }),
      queryInterface.addColumn('Bots', 'updatedAt', {
        allowNull: false,
        defaultValue: Sequelize.fn('now'),
        type: Sequelize.DATE,
      }),
      queryInterface.addColumn('Exchanges', 'createdAt', {
        allowNull: false,
        defaultValue: Sequelize.fn('now'),
        type: Sequelize.DATE,
      }),
      queryInterface.addColumn('Exchanges', 'updatedAt', {
        allowNull: false,
        defaultValue: Sequelize.fn('now'),
        type: Sequelize.DATE,
      }),
      queryInterface.addColumn('Strategies', 'createdAt', {
        allowNull: false,
        defaultValue: Sequelize.fn('now'),
        type: Sequelize.DATE,
      }),
      queryInterface.addColumn('Strategies', 'updatedAt', {
        allowNull: false,
        defaultValue: Sequelize.fn('now'),
        type: Sequelize.DATE,
      }),
      queryInterface.addColumn('StrategySegments', 'createdAt', {
        allowNull: false,
        defaultValue: Sequelize.fn('now'),
        type: Sequelize.DATE,
      }),
      queryInterface.addColumn('StrategySegments', 'updatedAt', {
        allowNull: false,
        defaultValue: Sequelize.fn('now'),
        type: Sequelize.DATE,
      }),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('Users', 'createdAt'),
      queryInterface.removeColumn('Users', 'updatedAt'),
      queryInterface.removeColumn('Bots', 'createdAt'),
      queryInterface.removeColumn('Bots', 'updatedAt'),
      queryInterface.removeColumn('Exchanges', 'createdAt'),
      queryInterface.removeColumn('Exchanges', 'updatedAt'),
      queryInterface.removeColumn('Strategies', 'createdAt'),
      queryInterface.removeColumn('Strategies', 'updatedAt'),
      queryInterface.removeColumn('StrategySegments', 'createdAt'),
      queryInterface.removeColumn('StrategySegments', 'updatedAt'),
    ]);
  },
};

'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable('Signals', {
      id: {
        type: Sequelize.UUID,
        allowNull: false,
        autoIncrement: false,
        defaultValue: Sequelize.literal('gen_random_uuid()'),
        unique: true,
        primaryKey: true,
      },
      analysisTime: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      exchangeName: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      indicatorName: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      isRequired: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      params: {
        type: Sequelize.JSON,
        allowNull: false,
      },
      signalResult: {
        type: Sequelize.ENUM([
          'Buy',
          'BuyToCover',
          'None',
          'Sell',
          'SellToCover',
        ]),
        allowNull: false,
      },
      ticker: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        defaultValue: Sequelize.fn('now'),
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        defaultValue: Sequelize.fn('now'),
        type: Sequelize.DATE,
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Signals');
  },
};

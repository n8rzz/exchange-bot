'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable('Bots', {
      id: {
        type: Sequelize.UUID,
        allowNull: false,
        autoIncrement: false,
        defaultValue: Sequelize.literal('gen_random_uuid()'),
        unique: true,
        primaryKey: true,
      },
      isActive: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
      isPaper: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      maxAllocationAmount: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      maxOpenBuyOrderTime: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 5,
      },
      maxOpenPositions: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 5,
      },
      maxOpenSellOrderTime: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 5,
      },
      minOrderAmount: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      numberOfTargets: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 3,
      },
      shouldNotifyOnCancel: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
      shouldNotifyOnError: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
      shouldNotifyOnTrade: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
      tickers: {
        type: Sequelize.ARRAY(Sequelize.STRING),
        allowNull: true,
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Bots');
  },
};

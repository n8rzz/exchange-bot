'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('UserExchangeAccounts', 'createdAt', {
        allowNull: false,
        defaultValue: Sequelize.fn('now'),
        type: Sequelize.DATE,
      }),
      queryInterface.addColumn('UserExchangeAccounts', 'updatedAt', {
        allowNull: false,
        defaultValue: Sequelize.fn('now'),
        type: Sequelize.DATE,
      }),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('UserExchangeAccounts', 'createdAt'),
      queryInterface.removeColumn('UserExchangeAccounts', 'updatedAt'),
    ]);
  },
};

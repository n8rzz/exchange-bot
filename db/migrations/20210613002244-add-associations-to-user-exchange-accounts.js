'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('UserExchangeAccounts', 'exchangeId', {
        type: Sequelize.UUID,
        references: {
          model: 'Exchanges',
          key: 'id',
          foreignKey: false,
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE',
        },
      }),
      queryInterface.addColumn('UserExchangeAccounts', 'userId', {
        type: Sequelize.UUID,
        references: {
          model: 'Users',
          key: 'id',
          foreignKey: false,
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE',
        },
      }),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('UserExchangeAccounts', 'exchangeId'),
      queryInterface.removeColumn('UserExchangeAccounts', 'userId'),
    ]);
  },
};

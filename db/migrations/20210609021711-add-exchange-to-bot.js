'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('Bots', 'exchangeId', {
      type: Sequelize.UUID,
      references: {
        model: 'Exchanges',
        key: 'id',
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('Bots', 'exchangeId');
  },
};

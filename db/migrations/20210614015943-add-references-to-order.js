'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('Orders', 'botId', {
        type: Sequelize.UUID,
        references: {
          model: 'Bots',
          key: 'id',
          foreignKey: false,
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE',
        },
      }),
      queryInterface.addColumn('Orders', 'exchangeId', {
        type: Sequelize.UUID,
        references: {
          model: 'Exchanges',
          key: 'id',
          foreignKey: false,
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE',
        },
      }),
      queryInterface.addColumn('Orders', 'userId', {
        type: Sequelize.UUID,
        references: {
          model: 'Users',
          key: 'id',
          foreignKey: false,
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE',
        },
      }),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('Orders', 'botId'),
      queryInterface.removeColumn('Orders', 'exchangeId'),
      queryInterface.removeColumn('Orders', 'userId'),
    ]);
  },
};

'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable('StrategySegments', {
      id: {
        type: Sequelize.UUID,
        allowNull: false,
        autoIncrement: false,
        defaultValue: Sequelize.literal('gen_random_uuid()'),
        unique: true,
        primaryKey: true,
      },

      indicator: {
        type: Sequelize.ENUM([
          'Absolute Price Oscillator',
          'Exponential Moving Average',
          'Simple Moving Average',
          'Relative Strength Indicator',
        ]),
        allowNull: false,
      },

      isRequired: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },

      keepSignalLength: {
        type: Sequelize.INTEGER,
        defaultValue: 1,
        allowNull: false,
      },

      params: {
        type: Sequelize.JSON,
        allowNull: false,
      },

      period: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },

      type: {
        type: Sequelize.ENUM([
          'Buy',
          'BuyToCover',
          'None',
          'Sell',
          'SellToCover',
        ]),
        allowNull: false,
      },

      // @ForeignKey(() => Strategy)
      // @Column({
      //   type: DataType.INTEGER,
      //   allowNull: false,
      // })
      // strategyId: number;

      // @HasMany(() => Signal)
      // signals: Signal;
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('StrategySegments');
  },
};

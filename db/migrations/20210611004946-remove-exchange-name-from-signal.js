'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Signals', 'exchangeName');
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Signals', 'exchangeName', {
      type: Sequelize.STRING,
      allowNull: false,
    });
  },
};

'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('Exchanges', 'apiKey'),
      queryInterface.removeColumn('Exchanges', 'password'),
      queryInterface.removeColumn('Exchanges', 'secret'),
      queryInterface.removeColumn('Exchanges', 'uid'),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('Exchanges', 'apiKey', {
        type: Sequelize.STRING,
        allowNull: true,
      }),
      queryInterface.addColumn('Exchanges', 'password', {
        type: Sequelize.STRING,
        allowNull: true,
      }),
      queryInterface.addColumn('Exchanges', 'secret', {
        type: Sequelize.STRING,
        allowNull: true,
      }),
      queryInterface.addColumn('Exchanges', 'uid', {
        type: Sequelize.STRING,
        allowNull: true,
      }),
    ]);
  },
};

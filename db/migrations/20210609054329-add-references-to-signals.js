'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('Signals', 'botId', {
        type: Sequelize.UUID,
        references: {
          model: 'Bots',
          key: 'id',
          foreignKey: false,
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE',
        },
      }),
      queryInterface.addColumn('Signals', 'strategySegmentId', {
        type: Sequelize.UUID,
        references: {
          model: 'StrategySegments',
          key: 'id',
          foreignKey: false,
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE',
        },
      }),
      queryInterface.addColumn('Signals', 'userId', {
        type: Sequelize.UUID,
        references: {
          model: 'Users',
          key: 'id',
          foreignKey: false,
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE',
        },
      }),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('Signals', 'botId'),
      queryInterface.removeColumn('Signals', 'strategySegmentId'),
      queryInterface.removeColumn('Signals', 'userId'),
    ]);
  },
};

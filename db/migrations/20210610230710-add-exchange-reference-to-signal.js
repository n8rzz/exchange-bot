'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Signals', 'exchangeId', {
      type: Sequelize.UUID,
      references: {
        model: 'Exchanges',
        key: 'id',
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Signals', 'exchangeId');
  },
};
